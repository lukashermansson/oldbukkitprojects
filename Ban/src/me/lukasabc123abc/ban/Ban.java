package me.lukasabc123abc.ban;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerPreLoginEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Ban extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Ban plugin;
	
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		getServer().getPluginManager().registerEvents(this, this);
	}


	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerPreLoginEvent event){
		System.out.println("1");

	    OfflinePlayer target = Bukkit.getOfflinePlayer(event.getName());
		if(target.isBanned()){
			System.out.println("2");
			String reason = event.getKickMessage();
			String message = "&4You are banned from &cmaster&bMine &r\n"
					+ reason + "&r\n"
					+ "&6Ban apeal at http://mastermineprison.enjin.com/forum/ &r\n";
			String formated = ChatColor.translateAlternateColorCodes('&', message);
			event.disallow(org.bukkit.event.player.PlayerPreLoginEvent.Result.KICK_BANNED, formated);
			System.out.println("3");
		}
	}
	
}	
