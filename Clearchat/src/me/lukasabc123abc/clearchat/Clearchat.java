package me.lukasabc123abc.clearchat;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Clearchat extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Clearchat plugin;
	private String prefix = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine" + ChatColor.GRAY + ":";
	@SuppressWarnings("unused")
	private String prefix2 = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine";
	boolean Muted;
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		Muted = false;
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		getServer().getPluginManager().registerEvents(this, this);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        //Broadcast Message
        if(commandLabel.equalsIgnoreCase("cgc")){
            if(!sender.hasPermission("mastermine.cgc")) {
            	sender.sendMessage(prefix + ChatColor.RED + " You don't have premission to clear chat");
            }else{
            	 for (Player player : getServer().getOnlinePlayers()){
                	for(int count = 1; count <= 105; count++){
                		player.sendMessage(" ");
                	}
                	player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + ChatColor.BOLD + "Master" +
                	ChatColor.RED + ChatColor.BOLD + "Mine" + ChatColor.BLACK + "]" + ChatColor.GREEN + " Chat has been cleared!");
            	}
            }
        } 
        else if(commandLabel.equalsIgnoreCase("gcm")){
            if(!sender.hasPermission("mastermine.gcm")) {
            	sender.sendMessage(prefix + ChatColor.RED + " You don't have premission to mute chat");
            }else{
            
            	
            	if (Muted){
            		Bukkit.broadcastMessage(prefix + ChatColor.GREEN + " chat has been re-enabeld.");
            	}else{
            		Bukkit.broadcastMessage(prefix + ChatColor.RED + " Chat has been disabled.");
            	}
            	Muted = !Muted;
            }
         }
		return false;
}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		
		if(Muted){
			
			if(event.getPlayer().hasPermission("mastermine.bypassmute")){
			}else{
			event.setCancelled(true);
			event.getPlayer().sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + ChatColor.BOLD + "Master" +
            ChatColor.RED + ChatColor.BOLD + "Mine" + ChatColor.BLACK + "]" + ChatColor.RED + " Chat is disabled!");
			}
		}else{
			
		}

		
		
	}
}	
