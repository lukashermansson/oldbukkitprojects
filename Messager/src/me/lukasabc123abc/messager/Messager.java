package me.lukasabc123abc.messager;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Messager extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Messager plugin;
	private String prefix = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine" + ChatColor.GRAY + ":";
	private String prefix2 = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine";
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
	}
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        //Broadcast Message
        if(commandLabel.equalsIgnoreCase("mastermineglobal") || commandLabel.equalsIgnoreCase("mmg")){
            if(!sender.hasPermission("mastermine.mastermineglobal")) {
        		Player player = (Player)sender;
                player.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
                if(args.length == 0) {
                	if(!(sender instanceof Player)){
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " Usage:" + ChatColor.GREEN + "/" + ChatColor.GREEN + commandLabel + ChatColor.GREEN+" {Message}");
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + "] " + "premmision: mastermine.mastermineglobal");
                	}else{
                		Player player = (Player)sender;
                		player.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " Usage:" + ChatColor.GREEN + "/" + ChatColor.GREEN + commandLabel + ChatColor.GREEN+" {Message}");
                	}
                }else {
                    String message = "";
                    for (String part : args) {
                            if (message != "") message += " ";
                            message += part;
                         }if(!(sender instanceof Player)){
                	    Bukkit.getServer().broadcastMessage("" + ChatColor.BLACK + ChatColor.BOLD + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + ChatColor.BOLD + "]" + ChatColor.GRAY + ": "  + ChatColor.AQUA + ChatColor.translateAlternateColorCodes('&', message));
                	
                }else{
                    Bukkit.getServer().broadcastMessage("" + ChatColor.BLACK + ChatColor.BOLD + "[" + ChatColor.DARK_RED + ChatColor.BOLD + prefix2 + ChatColor.BLACK + ChatColor.BOLD + "]" + ChatColor.GRAY + ": " + ChatColor.AQUA + ChatColor.translateAlternateColorCodes('&', message));
                }
                }

        }
        else if(commandLabel.equalsIgnoreCase("me")){
            if(!sender.hasPermission("mastermine.me")) {
        		Player player = (Player)sender;
                player.sendMessage(prefix + " You Don't Have Permission!");
                return true;
            }
                if(args.length == 0) {
                	if(!(sender instanceof Player)){
                    sender.sendMessage(prefix + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /me {Message}");
                	}else{
                		Player player = (Player)sender;
                        player.sendMessage(prefix + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /me {Message}");
                	}
                }else {
                    String message = "";
                    for (String part : args) {
                            if (message != "") message += " ";
                            message += part;
                         }if(!(sender instanceof Player)){
                	    Bukkit.getServer().broadcastMessage(prefix + ChatColor.DARK_GRAY + " [" + ChatColor.RED + "*" + ChatColor.DARK_GRAY + "] " +
                         "[" + ChatColor.RED + "Console" + ChatColor.DARK_GRAY + "]: " +
                	    		ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', message));
                	
                }else{
            	    Bukkit.getServer().broadcastMessage(prefix + ChatColor.DARK_GRAY + " [" + ChatColor.RED + "*" + ChatColor.DARK_GRAY + "] " +
                            "[" + ChatColor.RED + sender.getName() + ChatColor.DARK_GRAY + "]: " +
                   	    		ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', message));
                }
                }

        }       
        else if(commandLabel.equalsIgnoreCase("say")){
                if(!sender.hasPermission("mastermine.say")) {
        		Player player = (Player)sender;
                player.sendMessage(prefix + " You Don't Have Permission!");
                return true;
            }
                if(args.length == 0) {
                	if(!(sender instanceof Player)){
                    sender.sendMessage(prefix + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /say {Message}");
                	}else{
                		Player player = (Player)sender;
                        player.sendMessage(prefix + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /say {Message}");
                	}
                }else {
                    String message = "";
                    for (String part : args) {
                            if (message != "") message += " ";
                            message += part;
                         }if(!(sender instanceof Player)){
                	    Bukkit.getServer().broadcastMessage(prefix + ChatColor.DARK_GRAY + " [" + ChatColor.RED + "Say" + ChatColor.DARK_GRAY + "] " +
                         "[" + ChatColor.RED + "SERVER" + ChatColor.DARK_GRAY + "]: " +
                	    		ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', message));
                	
                }else{
            	    Bukkit.getServer().broadcastMessage(prefix + ChatColor.DARK_GRAY + " [" + ChatColor.RED + "Say" + ChatColor.DARK_GRAY + "] " +
                            "[" + ChatColor.RED + sender.getName() + ChatColor.DARK_GRAY + "]: " +
                   	    		ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', message));
                }
                }

        }
		return false;
}
}
		
