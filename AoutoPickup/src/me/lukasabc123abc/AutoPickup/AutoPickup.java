package me.lukasabc123abc.AutoPickup;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import me.lukasabc123abc.AutoPickup.Event;

public class AutoPickup extends JavaPlugin {
	public Plugin plugin;
	public final Logger logger = Logger.getLogger("Minecraft");
	
	@Override
	public void onEnable(){
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info("[" + pdfFile.getName() + "version" + pdfFile.getVersion() + "] " + " has been enabled.");
		new Event(this);
		File file = new File("plugins" + File.separator + "MasterMine"+File.separator+"fortune.yml");
		if(file.exists()){
			try {
				file.createNewFile();
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "created file fortune.yml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}
	
	@Override
	public void onDisable(){
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info("[" + pdfFile.getVersion() + "] " + pdfFile.getName() + " has been disabled.");
	}
}
