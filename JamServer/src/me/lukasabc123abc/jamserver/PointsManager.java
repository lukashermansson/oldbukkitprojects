package me.lukasabc123abc.jamserver;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;


public class PointsManager implements Listener{
	@EventHandler
	public void onPlayerjoin(PlayerJoinEvent event){
		FileConfiguration config = null;
		event.setJoinMessage(ChatColor.GREEN + event.getPlayer().getName() + " Joined!");
		//create a file for the user 
	
		
        File file = new File("plugins"+File.separator+"players"+File.separator + event.getPlayer().getName() + ".yml");
        if(!file.exists()){
                try {
                    file.createNewFile();
                    System.out.println("File Created: "+ event.getPlayer().getName() + ".yml");
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[Server]" + " coud not create a new file for " + event.getPlayer().getName());
                    e.printStackTrace();
                }
                config = YamlConfiguration.loadConfiguration(file);
                config.set("Player.Name", event.getPlayer());
                config.set("Stats.points", 100);
                config.set("Stats.wins", 0);
                config.set("Stats.losses", 0);
                config.set("Stats.played", 0);
                config.set("Stats.draw", 0);
                
               
                try {
                    config.save(file);
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[Server]" + " coud not save a new file for " + event.getPlayer().getName());
                    e.printStackTrace();
                }
           }
     }
	
	
	
	 public void addpoints(Player player, int value){
		 FileConfiguration config = null;
		 File file = new File("plugins"+File.separator+"players"+File.separator + player.getName() + ".yml");
		 config = YamlConfiguration.loadConfiguration(file);
		 config.set("Stats.points", config.getInt("Stats.points") + value);
		 try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	 public void removepoints(Player player, int value){
		 FileConfiguration config1 = null;
		 File file1 = new File("plugins"+File.separator+"players"+File.separator + player.getName() + ".yml");
		 config1 = YamlConfiguration.loadConfiguration(file1);
		 if(config1.getInt("Stats.points") - value >= 0){
			 config1.set("Stats.points", config1.getInt("Stats.points") - value);
		 }
		 try {
			config1.save(file1);

		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	 
	 
	 @SuppressWarnings("unused")
	@EventHandler
	 public void ondeth(PlayerDeathEvent event){
	
	     if(event.getEntity().getKiller() instanceof Player){
	    	 FileConfiguration configkiller = null;
			 File killer = new File("plugins"+File.separator+"players"+File.separator + event.getEntity().getKiller() + ".yml");
			 configkiller = YamlConfiguration.loadConfiguration(killer);
			 
			 FileConfiguration configdeth = null;
			 File deth = new File("plugins"+File.separator+"players"+File.separator + event.getEntity().getPlayer().getName() + ".yml");
			 configdeth = YamlConfiguration.loadConfiguration(deth);
	    	 //if(check if player anbd killer is both in an arena){
	    	 int killerpoints = configkiller.getInt("Stats.points");
	    	 addpoints(event.getEntity().getKiller(), (configdeth.getInt("Stats.points")/20));
	    	 removepoints(event.getEntity().getPlayer(),  (configdeth.getInt("Stats.points")/20));
	    	 String killmessage = "&7You have recieved &a" + (configdeth.getInt("Stats.points")/20)  + " points &7 for killing &c" + event.getEntity().getName();
	    	 String dethmessage = "&7You have lost &a" + (configdeth.getInt("Stats.points")/20)  + " points &7by being killed by &c" + event.getEntity().getPlayer().getKiller().getName();
	    	 event.getEntity().getPlayer().getKiller().sendMessage(ChatColor.translateAlternateColorCodes('&', killmessage));
	    	 event.getEntity().getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', dethmessage));
	    	 event.setDeathMessage("");
	    	 //}
	     }
	 }
	 @EventHandler(priority = EventPriority.HIGHEST)
	 public void onPlayerChatEvent(AsyncPlayerChatEvent event){
		 FileConfiguration configdeth = null;
		 File deth = new File("plugins"+File.separator+"players"+File.separator + event.getPlayer().getName() + ".yml");
		 configdeth = YamlConfiguration.loadConfiguration(deth);
		 event.setFormat(event.getFormat().replace("Ipoint", configdeth.getInt("Stats.points") + ""));
	 }

}
