package me.lukasabc123abc.jamserver;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


	public class Gui implements Listener{

		 
		public static ItemStack a1v1(){
			ItemStack a1v1 = new ItemStack(Material.DIAMOND_SWORD);
			//define your itemmeta here
	    	List<String> Lore = new ArrayList<String>();
	    	ItemMeta im = a1v1.getItemMeta();
	    	im.setDisplayName(ChatColor.YELLOW + "1v1");
	    	Lore.add(ChatColor.AQUA + "Challenge players online");
	    	Lore.add(ChatColor.AQUA + "with a good matchmaking system!");
	    	im.setLore(Lore);
	    	a1v1.setItemMeta(im);
			 
			//return the item

			return a1v1;
			 
			}
		  public static ItemStack Buy(){
			ItemStack Buy = new ItemStack(Material.EMERALD_BLOCK);
			//define your itemmeta here
	    	List<String> Lore = new ArrayList<String>();
	    	ItemMeta im = Buy.getItemMeta();
	    	im.setDisplayName(ChatColor.GOLD + "Buy");
	    	Lore.add(ChatColor.AQUA + "Click here to view all server ranks");
	    	im.setLore(Lore);
	    	Buy.setItemMeta(im);
			 
			//return the item

			return Buy;
			 
			}
		    public static void openpotGUI(Player p, int rows)
		    {
			    //make inv
		        Inventory gamemenu = Bukkit.createInventory(p, rows*9,ChatColor.LIGHT_PURPLE + "Game menu");
		        gamemenu.setItem(0, Gui.a1v1());
		        gamemenu.setItem(8, Gui.Buy());
		        p.openInventory(gamemenu);
		    }
		    @EventHandler
		    public void onClick(InventoryClickEvent e)
		    {
		        if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Game menu"))
		        {
		            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != null)
		            {
		                e.setCancelled(true);
		                if(e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().hasDisplayName())
		                {
		                    if(e.getCurrentItem().getItemMeta().getDisplayName().endsWith("v1")){
		                    	Player player = (Player) e.getWhoClicked();
		                    	player.sendMessage("you enterd que for 1v1");
		                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().endsWith("Buy")){
		                    	((Player) e.getWhoClicked()).chat("/buy");
		                    }
		                }
		            }
		        }
		    }
}
