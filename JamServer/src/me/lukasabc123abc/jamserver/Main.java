package me.lukasabc123abc.jamserver;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.lukasabc123abc.jamserver.Arenas.ArenaManager;
import me.lukasabc123abc.jamserver.Arenas.arena1;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	static Plugin plugin;
	public void onDisable() 
	{
		plugin = null;
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&8[&c&lZoX&8] &cShutting down &eZoneMC hub plugin..."));
		
	}
	

	@Override
	public void onEnable() {
		registerEvents(this, new PointsManager(), new Gui(), new Chat(), new arena1());
		plugin = this;
		getCommand("chat").setExecutor(new Chat());
		getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
		"&8[&c&lZoX&8] &abooting up &eZoneMC hub plugin..."));
	}    
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
		Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
		}


	@SuppressWarnings("deprecation")
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
      if(commandLabel.equalsIgnoreCase("stats")){
            if(!sender.hasPermission("zonemc.stats")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            Player target = Bukkit.getServer().getPlayer(args[0]);
			if(target != null) {
                //load player config
		        File file = new File("plugins"+File.separator+"players"+File.separator+ target.getName()+".yml");
		        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
		        //load levels file

				
		        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &bStats of " + target.getName()));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &ePoints : &c" + config1.getInt("Stats.points")));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eRanked 1v1 Stats : &a" + config1.getInt("Stats.wins") + "&8/&6" + config1.getInt("Stats.draw") + "&8/&4" + config1.getInt("Stats.losses")));
                }else {
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "ZoneMc" + ChatColor.BLACK + "] " + ChatColor.RED + "Player offline!");
                    return true;
                }
            }else{
            	//load player config
		        File file = new File("plugins"+File.separator+"players"+File.separator+ sender.getName()+".yml");
		        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);

				
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &bStats of " + sender.getName()));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &ePoints : &c" + config1.getInt("Stats.points")));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eRanked 1v1 Stats : &a" + config1.getInt("Stats.wins") + "&8/&6" + config1.getInt("Stats.draw") + "&8/&4" + config1.getInt("Stats.losses")));
            }
            }else if(commandLabel.equalsIgnoreCase("utilmatch")){
            	ArenaManager.utilmatch(Bukkit.getPlayer(args[0]), Bukkit.getPlayer(args[1]));
            }else if(commandLabel.equalsIgnoreCase("staff")){
            	if(sender.hasPermission("zonemc.staff")){
            	String message = "";
                for (String part : args) {
                        if (message != "") message += " ";
                        message += part;
                     }if(!(sender instanceof Player)){
            	     for(Player p: Bukkit.getOnlinePlayers()){
            	    	 if(p.hasPermission("zonemc.staff")){
            	    		 p.sendMessage(ChatColor.translateAlternateColorCodes('&', 
            	    		 "&8[&c&lZoX&8] &cConsole: &7" + ChatColor.translateAlternateColorCodes('&', message)));
            	    		 
            	    	 }
            	     }
            	     Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', 
            	    		 "&8[&c&lZoX&8] &cConsole: &7" + ChatColor.translateAlternateColorCodes('&', message)));
            }else{
            	for(Player p: Bukkit.getOnlinePlayers()){
            	 if(p.hasPermission("ZoneMC.staff")){
    	    		 p.sendMessage(ChatColor.translateAlternateColorCodes('&', 
    	    		 "&8[&c&lZoX&8] &c" + sender.getName() + " &7" + ChatColor.translateAlternateColorCodes('&', message)));
    	    	 }
            	}
            	Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', 
       	    		 "&8[&c&lZoX&8] &c" + sender.getName() + " &7" + ChatColor.translateAlternateColorCodes('&', message)));
            }
            }
            }else if(commandLabel.equalsIgnoreCase("Vansih")){
            	Player player = (Player) sender;
                for (Player other : getServer().getOnlinePlayers()) {
                    other.hidePlayer(player);
            }
                
		return false;
        }
	return false;
	}
	
	@EventHandler
	public void rightclick(PlayerInteractEvent event){
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(!(event.getPlayer().getItemInHand() == null)){
				if(event.getPlayer().getItemInHand().getType() == Material.COMPASS){
					Gui.openpotGUI(event.getPlayer(), 1);
				}
			}
		}
	}
	
	public static ItemStack compass(){
		ItemStack compass = new ItemStack(Material.COMPASS);
		//define your itemmeta here
    	List<String> Lore = new ArrayList<String>();
    	ItemMeta im = compass.getItemMeta();
    	im.setDisplayName("" + ChatColor.YELLOW + ChatColor.BOLD + "Game Menu");
    	Lore.add(ChatColor.AQUA + "Right click to view all gamemodes");
      	im.setLore(Lore);
    	compass.setItemMeta(im);
		 
		//return the item

		return compass;
		 
		}
	@EventHandler
	public void onTab(PlayerJoinEvent event) throws IOException{
		Player player = event.getPlayer();
		if(player.hasPermission("tab.owner")){
		player.setPlayerListName(ChatColor.YELLOW + player.getName());
		}else if(player.hasPermission("tab.dev")){
		player.setPlayerListName(ChatColor.AQUA + player.getName());
		}else if(player.hasPermission("tab.vip")){
		player.setPlayerListName(ChatColor.DARK_PURPLE + player.getName());
		}else if(player.hasPermission("tab.mod")){
			player.setPlayerListName(ChatColor.RED + player.getName());
		}else if(player.hasPermission("tab.srmod")){
			player.setPlayerListName(ChatColor.DARK_RED + player.getName());
		}
		
		
		player.getInventory().setItem(0, compass());
		
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta bm = (BookMeta) book.getItemMeta();
		bm.setAuthor(ChatColor.translateAlternateColorCodes('&', "&eZoneMC"));
		bm.addPage();
		bm.setTitle(ChatColor.translateAlternateColorCodes('&', "&e&lZoneMC Guide"));
		book.setItemMeta(bm);
		player.getInventory().setItem(1, book);
		
		World w = Bukkit.getWorld("hub");
		player.teleport(w.getSpawnLocation());
		
		if(!player.hasPermission("Zonemc.adventure")){
			player.setGameMode(GameMode.ADVENTURE);
		}
	}


	public static Plugin getPlugin() {
		return plugin;
	}
	
	@EventHandler
	public void onfooddecay(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	public void Onpickup(PlayerPickupItemEvent event){
		if(!event.getPlayer().hasPermission("zonemc.pickup")){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
	 
		if(!event.getPlayer().hasPermission("zonemc.drop")){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event){
	 
		if(!event.getWhoClicked().hasPermission("zonemc.drag")){
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void swear(AsyncPlayerChatEvent event){
		if(!event.getPlayer().hasPermission("zonemc.swear")){
		String[] swearWords = {"fuck", "pussy", "dick", "cock", "cunt", "kuk", "fitta", "snopp", "fml", "shit", "niglet", "nigger",  "wank", "bastard", "faggot", "fag", "this server sucks", "wtf"};
		
		for(String word: swearWords){
			 if(word == "wtf"){
				 event.setMessage(event.getMessage().replaceAll("(?i)" + word, "what the fruit"));
			  }else if(word.length() == 4){
		       event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%&"));
			  }else if(word.length() == 5){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%!&"));
			  }else if(word == "fml"){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "Forge mod loader"));
			  }else if(word.length() == 3){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#&@"));
			  }else if(word == "this server sucks"){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "this is the best server ever"));
			  }else{
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%&$!"));
			  }
		}
	}
	}
	
}
