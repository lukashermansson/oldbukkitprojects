package me.lukasabc123abc.jamserver.Arenas;

import me.lukasabc123abc.jamserver.Main;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class arena1 implements Listener{
	static Player pl1 = null;
	static Player pl2 = null;
	static boolean running = false;
	public static void startarena(Player p1, Player p2) {
		startTimer();
		pl1 = p1;
		pl2 = p2;
	}
	static int left;
	static int stage = 1;
	public static void startTimer() {
		running = true;
		left = 60*3;
	new Runnable() {
	public int arena1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), this, 0L, 20L);
	@Override
	public void run() {
	if (left > 0) {
	left--;
	switch (left) {
	case 60*3:
	Bukkit.broadcastMessage("There are 3 minutes remaining!");
	break;
	case 60*2:
	Bukkit.broadcastMessage("There are 2 minutes remaining!");
	break;
	case 60:
	Bukkit.broadcastMessage("There is 1 minute remaining!");
	break;
	case 30:
	Bukkit.broadcastMessage("There are 30 seconds remaining!");
	break;
	case 10:
	Bukkit.broadcastMessage("There are 10 seconds remaining!");
	break;
	case 3:
	Bukkit.broadcastMessage("There are 3 seconds remaining!");
	break;
	case 2:
	Bukkit.broadcastMessage("There are 2 seconds remaining!");
	break;
	case 1:
	Bukkit.broadcastMessage("There are 1 seconds remaning!");
	break;
	}
	 
	} else {
	Bukkit.getScheduler().cancelTask(arena1);
	EndGame();
	}
	}
	};
	}

	public static void EndGame() {
		running = false;
		Bukkit.getServer().broadcastMessage("arena1 ended");
		World w = Bukkit.getWorld("1v1lobby");
		if (w == null) {
		    return;
		} else {
		    pl1.teleport(w.getSpawnLocation());
		    pl2.teleport(w.getSpawnLocation());
		}

		if(stage == 1){
			startarena(pl1, pl2);
		}else if(stage == 2){
			startarena(pl1, pl2);
		}else if(stage == 3){
			startarena(pl1, pl2);
		}else{
			stage = 1;
		}
		stage++;
		//no code after this 
		pl1 = null;
		pl2 = null;
	}
	@EventHandler
	public void dethevent(PlayerDeathEvent event){
		if(event.getEntity() == pl1 || event.getEntity() == pl2){
			left = 0;
			
		}
	}
}
