package me.lukasabc123abc.ChatColors;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;


public class PlayerListener implements Listener {
	public PlayerListener(event plugin){
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@EventHandler
	public void onspeak(AsyncPlayerChatEvent e){
		String message = e.getMessage();
		String colored = ChatColor.translateAlternateColorCodes('&', message);
		e.setMessage(colored);
	}
	
}
