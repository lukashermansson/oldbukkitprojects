package me.lukasabc123abc.join;


import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class PlayerListener implements Listener {
	public PlayerListener(event plugin){
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if(!player.hasPlayedBefore()){
			e.setJoinMessage("");
	    }else {
	    	if(e.getPlayer().hasPermission("join.owner")){
	    	String message = "&e" + e.getPlayer().getName() + " &5Has joined the server!";
	    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.dev")){
	    		String message = "&b" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.mod")){
	    		String message = "&c" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.srmod")){
	    		String message = "&4" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else{
	    		e.setJoinMessage("");
	    	}
	    }
	}
	@EventHandler
	public void onleave(PlayerQuitEvent e){
		e.setQuitMessage("");
	}
	
}
