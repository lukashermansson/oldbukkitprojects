package me.lukasabc123abc.join;


import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class PlayerListener implements Listener {
	public PlayerListener(event plugin){
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if(!player.hasPlayedBefore()){
			e.setJoinMessage(ChatColor.BLACK + "[" + ChatColor.GREEN + "+" + ChatColor.BLACK + "] " + ChatColor.AQUA + "Welcome " + ChatColor.WHITE + e.getPlayer().getName() + ChatColor.AQUA + " to " + ChatColor.BOLD + ChatColor.DARK_BLUE +"Master" +ChatColor.DARK_RED +"Mine!");
	    }else {
	    	if(e.getPlayer().hasPermission("join.staff")){
	    	e.setJoinMessage(ChatColor.BLACK + "[" + ChatColor.GREEN + "+" + ChatColor.BLACK + "] " + ChatColor.DARK_RED +  e.getPlayer().getName());
	    	}else{
	    	e.setJoinMessage(ChatColor.BLACK + "[" + ChatColor.GREEN + "+" + ChatColor.BLACK + "] " + ChatColor.GRAY +  e.getPlayer().getName());
	    	}
	    }
	}
	@EventHandler
	public void onleave(PlayerQuitEvent e){
		e.setQuitMessage(ChatColor.BLACK + "[" + ChatColor.RED + "-" + ChatColor.BLACK + "] " + ChatColor.GRAY + e.getPlayer().getName());
		
	}
	
}
