package com.lukasabc123abc.game.GFX;

import java.util.Random;

public class Screen {

	private int width, height;
	public int[] pixels;
	
	public Screen(int width, int height){
		this.width = width;
		this.height = height;
		pixels = new int[width*height];
	}
	
	public void clear(){
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = 0;
			
		}
		
	}
	int time = 1;
	Random random = new Random();
	public void render(){
		time++;
		// one color
		int color = random.nextInt();
		for (int y = 0; y < height; y++){
			//int color = random.nextInt();
			for (int x = 0; x < width; x++){
				pixels[x + y * width] = color * time;

			}
		}
		
	}
	
}
