package com.lukasabc123abc.game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import com.lukasabc123abc.game.GFX.Screen;

public class Game extends Canvas implements Runnable{

	
	private static final long serialVersionUID = 1L;
	public static int width = 300;
	public static int height = width / 16 * 9;
	public static int scale = 3;
	private boolean running = false;
	private Screen screen;
	private JFrame frame;
	private Thread thread;
	static BufferedImage img = null;
	boolean DisplayingWarning = true;
	private BufferedImage  image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

	private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	public Game()  {
		Dimension size = new Dimension(width*scale, height*scale);
		setPreferredSize(size);
		
		screen = new Screen(width,height);
		frame = new JFrame();
	}
	public synchronized void start(){
		running = true;
		thread = new Thread(this, "display");
		thread.start();
		
	}
	public synchronized void stop(){
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		while(running == true){
			tick();
			render();
		}
		
	}
	int TickCount;
	public void tick(){
		//display warning 
		TickCount++;
		int seconds = 5;
		int time = seconds * 20;
		if(time > TickCount){
			//time has passed warning text
			DisplayingWarning = true;
		}else{
			
			
		}
		
	}
	
	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if (bs == null){
			createBufferStrategy(3);
			return;
		}
		
		for (int i = 0; i < pixels.length; i++){
			pixels[i] = screen.pixels[i];
		}
		Graphics g = bs.getDrawGraphics();
		//display inbetwen
		if(DisplayingWarning){
			g.drawImage(img, 0, 0, getWidth(), getHeight(),null);
		}else{
			screen.clear();
			screen.render();
			
		}
		//not after here
		g.dispose();
		bs.show();
	}
	
	public static void main(String[] args){
		try {
		    img = ImageIO.read(new File("Warning.png"));
		} catch (IOException e) {
		}
		final Game game = new Game();
		game.frame.setResizable(true);
		game.frame.setTitle("Epelepsy Sim");
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		
		game.start();
		
		
	}
	
	
}
