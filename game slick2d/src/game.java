import java.io.File;

import org.lwjgl.LWJGLUtil;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;


public class game {

	public static void main(String[] args) {
		System.setProperty("org.lwjgl.librarypath", new File(new File(System.getProperty("user.dir"), "native"), LWJGLUtil.getPlatformName()).getAbsolutePath());
	try {
		AppGameContainer app = new AppGameContainer( new screen() );
		app.setDisplayMode(800, 600, false);
		app.start();
	} catch (SlickException e) {
		e.printStackTrace();
	}
	}

}
