package me.lukasabc123abc.global;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Global extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Global plugin;
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        //Broadcast Message
        if(commandLabel.equalsIgnoreCase("global")){
            if(!sender.hasPermission("global.global")) {
        		Player player = (Player)sender;
                player.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
                if(args.length == 0) {
                	if(!(sender instanceof Player)){
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /global {Message}");
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "] " + "premmision: global.global");
                	}else{
                		Player player = (Player)sender;
                        player.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "] " + ChatColor.DARK_GRAY + ChatColor.BOLD + ">" + ChatColor.RED + " Usage:" + ChatColor.GREEN + " /global {Message}");
                	}
                }else {
                    String message = "";
                    for (String part : args) {
                            if (message != "") message += " ";
                            message += part;
                         }if(!(sender instanceof Player)){
                	    Bukkit.getServer().broadcastMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "]" + ChatColor.GRAY + ": "  + ChatColor.AQUA + ChatColor.translateAlternateColorCodes('&', message));
                	
                }else{
                    Bukkit.getServer().broadcastMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "Global" + ChatColor.BLACK + "]" + ChatColor.GRAY + ": " + ChatColor.AQUA + ChatColor.translateAlternateColorCodes('&', message));
                }
                }

        }
		return false;

}
	
}
		
