package me.lukasabc123abc.hide;
 
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;
 
public class Hide extends JavaPlugin implements Listener {
        private String prefix = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine" + ChatColor.GRAY + ":";
        @EventHandler
        public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
                if (e.getPlayer().hasPermission("hide.bypass")) {
                        return;
                }
                if(!e.getMessage().startsWith("/plotme") || !e.getMessage().startsWith("/plot")){
                if (e.getMessage().equals("/?") 
                	|| e.getMessage().equals("/pl") 
                	|| e.getMessage().equals("/plugins") 
                	|| e.getMessage().equals("/version") 
                	|| e.getMessage().equals("/bukkit:pl") 
                	|| e.getMessage().equals("/bukkit:plugins") 
                	|| e.getMessage().equals("/bukkit:version") 
                	|| e.getMessage().equals("/bukkit:ver") 
                	|| e.getMessage().equals("/ver") 
                	|| e.getMessage().equalsIgnoreCase("/bukkit:icanhasbukkit") 
                	|| e.getMessage().equals("/icanhasbukkit")) {
                        e.getPlayer().sendMessage(prefix + ChatColor.RED + " You don�t have Permissions to see the plugins or version! Your abuse has be logged and reported to the Admins!!");
                        e.setCancelled(true);
                }
        }
        }
       
        @Override
        public void onEnable() {
                Bukkit.getServer().getPluginManager().registerEvents(this, this);
        }
}