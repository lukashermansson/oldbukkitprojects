package me.lukasabc123abc.blockAnvil;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class BlockAnvil extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static BlockAnvil plugin;
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onPlayerClick(PlayerInteractEvent event){
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock().getType() == Material.ANVIL){
				if(event.getPlayer().hasPermission("blockanvil.bypass")){
					event.getPlayer().sendMessage(ChatColor.GREEN + "Anvils are currently disabled but you may use them");
				}else{
					event.setCancelled(true);
					event.getPlayer().sendMessage(ChatColor.DARK_RED + "Anvils are currently disabled becuse of a dupe bug/glitch");
				}
			}
		}
	}
	
}	
