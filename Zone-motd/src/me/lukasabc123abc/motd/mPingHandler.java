package me.lukasabc123abc.motd;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.md_5.bungee.api.ChatColor;
import me.bigteddy98.animatedmotd.bungee.Scroller;
import me.bigteddy98.animatedmotd.bungee.ping.ServerData;
import me.bigteddy98.animatedmotd.bungee.ping.StatusListener;

public class mPingHandler implements StatusListener{
    private final String servername = ChatColor.translateAlternateColorCodes('&', "         &5>>  &3ZoneMC.eu  &8| &b Beta Testing&5 <<");
    private final Scroller scroller2 = new Scroller(ChatColor.translateAlternateColorCodes('&', "&eFFA &8| &e1v1 &8| &eFactions"), 60, 10, '&');
	
    @Override
    public ServerData update() {
    	BufferedImage img = null;
		try {
			img = ImageIO.read(new File("server-icon.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

		
		
    	List<String> list = new ArrayList<String>();
    	list.add(ChatColor.translateAlternateColorCodes('&', "            &8[&c&lZoX&r&8]"));
		list.add(ChatColor.translateAlternateColorCodes('&', "&cTeamspeak&8: &bComming soon"));
		list.add(ChatColor.translateAlternateColorCodes('&', "&3Website&8: &bzonemc.eu"));
		list.add(ChatColor.translateAlternateColorCodes('&', "&5&m---------------------"));
        return new ServerData(this.servername, this.scroller2.next(), bimage ,300, ChatColor.translateAlternateColorCodes('&', "&7%COUNT%&8/&7%MAX%"), list);
    }
}
