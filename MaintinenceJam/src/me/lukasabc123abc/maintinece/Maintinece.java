package me.lukasabc123abc.maintinece;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedServerPing;

public class Maintinece extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Maintinece plugin;
	private String prefix = ChatColor.AQUA + "Chat" + ChatColor.RED + "Tweeks" + ChatColor.GRAY + ":";
	boolean Muted;
	
	private ProtocolManager protocolManager;
	  
	public void onLoad() {
	    protocolManager = ProtocolLibrary.getProtocolManager();
	}
		
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		Muted = false;
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		getServer().getPluginManager().registerEvents(this, this);
		
		FileConfiguration config = null;
		 File file = new File("plugins"+File.separator+"maintenance"+File.separator+"config.yml");
	        if(!file.exists()){
	                try {
	                    file.createNewFile();
	                } catch (IOException e) {
	                	Bukkit.getServer().getLogger().warning("coud not create a new config file");
	                    e.printStackTrace();
	                }
	                //write to file
	                config = YamlConfiguration.loadConfiguration(file);
	                config.set("messages.motd", "&cdefaultmotd");
	                config.set("messages.maintenance", "&cmaintinence mode");
	                config.set("option.on", false);
	                config.set("messages.kick", "&cwe are in maintinece");
	                config.set("messages.whitelistkick", "&4we are in mantinence");
	                try {
	                    config.save(file);
	                } catch (IOException e) {
	                	Bukkit.getServer().getLogger().warning( "coud not save the config file for the fist time");
	                    e.printStackTrace();
	                }
	        }
	        ProtocolManager prm = ProtocolLibrary.getProtocolManager();
		    prm.addPacketListener(new PacketAdapter(this, PacketType.Status.Server.OUT_SERVER_INFO){
		        @Override
		        public void onPacketSending(PacketEvent event) {
		            
		                WrappedServerPing ping = event.getPacket().getServerPings().read(0);
		                //System.out.println(ping.getVersionProtocol() +  "," + ping.getVersionName());
		                if(maintinence()){
		                	ping.setVersionProtocol(999);
		                	String version = "&4Maintinence Mode";
		                	ping.setVersionName(ChatColor.translateAlternateColorCodes('&', version));
		                }else if(!(ping.getVersionProtocol() >= 47)){
			                	ping.setVersionProtocol(5);
			                	String version = "&4Im on &a1.7.2&8-&a1.8";
			                	ping.setVersionName(ChatColor.translateAlternateColorCodes('&', version));
		                }else{
		                	
		                }
		           
		        }
		    });
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        if(commandLabel.equalsIgnoreCase("maintinece")){
        	if(args.length == 0){
            if(!sender.hasPermission("lukasabc123abc.maintinece")) {
            	sender.sendMessage(prefix + ChatColor.RED + " You don't have premission");
            }else{
            	File file = new File("plugins"+File.separator+"maintenance"+File.separator+"config.yml");
                FileConfiguration config = YamlConfiguration.loadConfiguration(file);
                if(config.getBoolean("option.on")){
                	config.set("option.on", false);
                	try {
						config.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
                	sender.sendMessage(ChatColor.GREEN + "Maininence mode has been toggled off");
                
                	Bukkit.setWhitelist(false);
                }else{
                	config.set("option.on", true);
                	try {
						config.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
                	sender.sendMessage(ChatColor.GREEN + "Maininence mode has been toggled on");
                	for (Player player : getServer().getOnlinePlayers()){
                		if(!player.isWhitelisted()){
                			String message = config.getString("messages.kick");
                			player.kickPlayer(ChatColor.translateAlternateColorCodes('&', message));
                		}
                	}
                	Bukkit.setWhitelist(true);
                }
            }
         }else if(args.length == 1){
        	 if(args[0].equalsIgnoreCase("reload")){
        		 if(!sender.hasPermission("lukasabc123abc.mreload")) {
                 	sender.sendMessage(prefix + ChatColor.RED + " You don't have premission");
                 }else{
                	  reloadConfig();
                      sender.sendMessage("maintinence was reloaded");
                 }
        	 }else{
        		 sender.sendMessage(ChatColor.RED + "not a valid option");
        	 }
         }else{
    		 sender.sendMessage(ChatColor.RED + "too meny arguments");
        }
        }
		return false;
}
	
	@EventHandler
	public void onpingserver(ServerListPingEvent event) {
		File file = new File("plugins"+File.separator+"maintenance"+File.separator+"config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		if(maintinence()){
			String motd = config.getString("messages.maintenance");
			motd.replaceAll("%n%", System.getProperty("line.separator"));
			event.setMotd(ChatColor.translateAlternateColorCodes('&', motd));
		}else{
			String motd = config.getString("messages.motd");
			event.setMotd(ChatColor.translateAlternateColorCodes('&', motd));
		}
		
		
	}

	private boolean maintinence() {
		File file = new File("plugins"+File.separator+"maintenance"+File.separator+"config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        if(config.getBoolean("option.on")){
        	return true;
        }else{
        	return false;
        }
		
	}
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event)
	{
	if ((!(Bukkit.getServer().hasWhitelist())) || (event.getPlayer().isWhitelisted()))
	return;
	if(maintinence()){
	File file = new File("plugins"+File.separator+"maintenance"+File.separator+"config.yml");
    FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	String message = config.getString("messages.whitelistkick");
	event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, ChatColor.translateAlternateColorCodes('&', message));
	}
	}

}	
