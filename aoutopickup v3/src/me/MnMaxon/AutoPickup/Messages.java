/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfigurationOptions;
/*    */ 
/*    */ public class Messages
/*    */ {
/*    */   public static String COMMAND_BLOCK;
/*    */   public static String COMMAND_BLOCK_FAIL;
/*    */   public static String COMMAND_SMELT;
/*    */   public static String COMMAND_SMELT_FAIL;
/*    */   public static String FULL_INVENTORY;
/*    */   public static String PREFIX;
/*    */ 
/*    */   public static void setUp()
/*    */   {
/* 14 */     Main.MessageConfig = new SuperYaml(Main.dataFolder + "/Messages.yml");
/* 15 */     easyConfig("Prefix", Boolean.valueOf(true));
/*    */ 
/* 17 */     easyConfig("Messages.Block Command", "Your inventory has been blocked!");
/* 18 */     easyConfig("Messages.Block Fail", "Your inventory had nothing to be blocked!");
/* 19 */     easyConfig("Messages.Smelt Command", "Your inventory has been smelted!");
/* 20 */     easyConfig("Messages.Smelt Fail", "Your inventory had nothing to be smelted!");
/* 21 */     easyConfig("Messages.Full Inventory", "You're inventory is full!");
/* 22 */     Main.MessageConfig.config.options().copyDefaults();
/* 23 */     Main.MessageConfig.save();
/*    */ 
/* 25 */     if (Main.MessageConfig.getBoolean("Use Prefix").booleanValue())
/* 26 */       PREFIX = ChatColor.WHITE + "[" + ChatColor.AQUA + "AutoPickup" + ChatColor.WHITE + "] " + ChatColor.RESET;
/*    */     else
/* 28 */       PREFIX = "";
/* 29 */     COMMAND_BLOCK = PREFIX + ChatColor.GREEN + Main.MessageConfig.getString("Messages.Block Command");
/* 30 */     COMMAND_BLOCK_FAIL = PREFIX + ChatColor.RED + Main.MessageConfig.getString("Messages.Block Fail");
/* 31 */     COMMAND_SMELT = PREFIX + ChatColor.GREEN + Main.MessageConfig.getString("Messages.Smelt Command");
/* 32 */     COMMAND_SMELT_FAIL = PREFIX + ChatColor.RED + Main.MessageConfig.getString("Messages.Smelt Fail");
/* 33 */     FULL_INVENTORY = PREFIX + ChatColor.RED + Main.MessageConfig.getString("Messages.Full Inventory");
/*    */   }
/*    */ 
/*    */   private static boolean easyConfig(String path, Object value) {
/* 37 */     if (Main.MessageConfig.get(path) == null) {
/* 38 */       Main.MessageConfig.set(path, value);
/* 39 */       return true;
/*    */     }
/* 41 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.Messages
 * JD-Core Version:    0.6.2
 */