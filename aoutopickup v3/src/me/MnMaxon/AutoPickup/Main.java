/*     */ package me.MnMaxon.AutoPickup;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import org.bukkit.Bukkit;
/*     */ import org.bukkit.ChatColor;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.Server;
/*     */ import org.bukkit.command.Command;
/*     */ import org.bukkit.command.CommandSender;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.inventory.InventoryType;
/*     */ import org.bukkit.inventory.Inventory;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ import org.bukkit.plugin.PluginManager;
/*     */ import org.bukkit.plugin.java.JavaPlugin;
/*     */ 
/*     */ public final class Main extends JavaPlugin
/*     */ {
/*     */   public static String dataFolder;
/*     */   public static Main plugin;
/*     */   public static SuperYaml MainConfig;
/*     */   public static SuperYaml MessageConfig;
/*  24 */   public static Map<Material, AutoBlockInfo> blocksToAuto = new HashMap();
/*  25 */   public static Map<Material, AutoBlockInfo> blocksToSmelt = new HashMap();
/*  26 */   public static boolean usingOtherDrops = false;
/*  27 */   public static boolean usingFortuneBlocks = false;
/*     */ 
/*     */   public void onEnable()
/*     */   {
/*  31 */     plugin = this;
/*  32 */     dataFolder = getDataFolder().getAbsolutePath();
/*  33 */     reloadConfigs();
/*  34 */     setUpSmeltBlocks();
/*  35 */     setUpAutoBlocks();
/*  36 */     getServer().getPluginManager().registerEvents(new MainListener(), this);
/*     */   }
/*     */ 
/*     */   private void setUpAutoBlocks()
/*     */   {
/*  63 */     blocksToAuto.put(Material.DIAMOND, new AutoBlockInfo(9, Material.DIAMOND_BLOCK, 1));
/*  64 */     blocksToAuto.put(Material.EMERALD, new AutoBlockInfo(9, Material.EMERALD_BLOCK, 1));
/*  65 */     blocksToAuto.put(Material.GOLD_INGOT, new AutoBlockInfo(9, Material.GOLD_BLOCK, 1));
/*  66 */     blocksToAuto.put(Material.GOLD_NUGGET, new AutoBlockInfo(9, Material.GOLD_INGOT, 1));
/*  67 */     blocksToAuto.put(Material.IRON_INGOT, new AutoBlockInfo(9, Material.IRON_BLOCK, 1));
/*  68 */     blocksToAuto.put(Material.INK_SACK, new AutoBlockInfo(9, Material.LAPIS_BLOCK, 1));
/*  69 */     blocksToAuto.put(Material.QUARTZ, new AutoBlockInfo(4, Material.QUARTZ_BLOCK, 1));
/*  70 */     blocksToAuto.put(Material.REDSTONE, new AutoBlockInfo(9, Material.REDSTONE_BLOCK, 1));
/*  71 */     blocksToAuto.put(Material.COAL, new AutoBlockInfo(9, Material.COAL_BLOCK, 1));
/*     */   }
/*     */ 
/*     */   private void setUpSmeltBlocks() {
/*  75 */     blocksToSmelt.put(Material.DIAMOND_ORE, new AutoBlockInfo(9, Material.DIAMOND, 1));
/*  76 */     blocksToSmelt.put(Material.EMERALD_ORE, new AutoBlockInfo(9, Material.EMERALD, 1));
/*  77 */     blocksToSmelt.put(Material.GOLD_ORE, new AutoBlockInfo(9, Material.GOLD_INGOT, 1));
/*  78 */     blocksToSmelt.put(Material.IRON_ORE, new AutoBlockInfo(9, Material.IRON_INGOT, 1));
/*     */   }
/*     */ 
/*     */   public static void reloadConfigs() {
/*  82 */     SuperYaml MainBefore = new SuperYaml(dataFolder + "/Config.yml");
/*  83 */     MainConfig = new SuperYaml(dataFolder + "/Config.yml");
/*  84 */     Messages.setUp();
/*  85 */     if (MainConfig.get("AutoBlock") != null)
/*  86 */       MainConfig.set("AutoBlock", null);
/*  87 */     if (MainConfig.get("AutoSmelt") != null)
/*  88 */       MainConfig.set("AutoSmelt", null);
/*  89 */     easyConfig("Permission.Reload", "AutoPickup.Reload");
/*  90 */     easyConfig("Permission.InfinitePick", "AutoPickup.InfinitePick");
/*  91 */     easyConfig("Permission.AutoBlock", "AutoPickup.AutoBlock");
/*  92 */     easyConfig("Permission.AutoSmelt", "AutoPickup.AutoSmelt");
/*  93 */     easyConfig("Permission.Commands.AutoSmelt", "AutoPickup.Commands.AutoSmelt");
/*  94 */     easyConfig("Permission.Commands.AutoBlock", "AutoPickup.Commands.AutoBlock");
/*  95 */     Permissions.AUTO_BLOCK = MainConfig.getString("Permission.AutoBlock");
/*  96 */     Permissions.AUTO_SMELT = MainConfig.getString("Permission.AutoSmelt");
/*  97 */     Permissions.COMMANDS_BLOCK = MainConfig.getString("Permission.Commands.AutoBlock");
/*  98 */     Permissions.COMMANDS_SMELT = MainConfig.getString("Permission.Commands.AutoSmelt");
/*  99 */     Permissions.RELOAD = MainConfig.getString("Permission.Reload");
/* 100 */     Permissions.INFINITE_PICK = MainConfig.getString("Permission.InfinitePick");
/*     */ 
/* 102 */     if (!MainConfig.equals(MainBefore))
/* 103 */       MainConfig.save();
/*     */   }
/*     */ 
/*     */   private static boolean easyConfig(String path, Object value) {
/* 107 */     if (MainConfig.get(path) == null) {
/* 108 */       MainConfig.set(path, value);
/* 109 */       return true;
/*     */     }
/* 111 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean onCommand(CommandSender s, Command cmd, String label, String[] args)
/*     */   {
/* 116 */     if ((s instanceof Player)) {
/* 117 */       Player p = (Player)s;
/* 118 */       if (cmd.getLabel().equalsIgnoreCase("AutoBlock")) {
/* 119 */         if (p.hasPermission(Permissions.COMMANDS_BLOCK)) {
/* 120 */           ItemStack[] newInvCont = convertToBlocks(p.getInventory().getContents());
/* 121 */           if (!p.getInventory().getContents().equals(newInvCont)) {
/* 122 */             p.getInventory().setContents(newInvCont);
/* 123 */             p.updateInventory();
/* 124 */             p.sendMessage(Messages.COMMAND_BLOCK);
/*     */           } else {
/* 126 */             p.sendMessage(Messages.COMMAND_BLOCK_FAIL);
/*     */           }
/*     */         } else { p.sendMessage(ChatColor.RED + "You do not have permission for this command!"); }
/* 129 */       } else if (cmd.getLabel().equalsIgnoreCase("AutoSmelt"))
/* 130 */         if (p.hasPermission(Permissions.COMMANDS_SMELT)) {
/* 131 */           ItemStack[] newInvCont = convertToBlocks(p.getInventory().getContents());
/* 132 */           for (int i = 0; i < newInvCont.length; i++) {
/* 133 */             if ((newInvCont[i] != null) && (blocksToSmelt.containsKey(newInvCont[i].getType())))
/* 134 */               newInvCont[i].setType(((AutoBlockInfo)blocksToSmelt.get(newInvCont[i].getType())).getNewType());
/*     */           }
/* 136 */           if (!p.getInventory().getContents().equals(newInvCont)) {
/* 137 */             p.getInventory().setContents(newInvCont);
/* 138 */             p.updateInventory();
/* 139 */             p.sendMessage(Messages.COMMAND_SMELT);
/*     */           } else {
/* 141 */             p.sendMessage(Messages.COMMAND_SMELT_FAIL);
/*     */           }
/*     */         } else { p.sendMessage(ChatColor.RED + "You do not have permission for this command!"); }
/*     */     }
/* 145 */     if (cmd.getLabel().equalsIgnoreCase("AutoPickup"))
/* 146 */       if ((!(s instanceof Player)) || (((Player)s).hasPermission(Permissions.RELOAD))) {
/* 147 */         reloadConfigs();
/* 148 */         s.sendMessage(ChatColor.GREEN + "Reload successful!");
/*     */       } else {
/* 150 */         s.sendMessage(ChatColor.RED + "You do not have permission for this command!");
/*     */       }
/* 151 */     return false;
/*     */   }
/*     */ 
/*     */   public static ArrayList<ItemStack> addToInventory(Player p, ArrayList<ItemStack> finalItems) {
/* 155 */     ArrayList remaining = new ArrayList();
/*     */     Iterator localIterator2;
/* 156 */     for (Iterator localIterator1 = finalItems.iterator(); localIterator1.hasNext(); 
/* 158 */       localIterator2.hasNext())
/*     */     {
/* 156 */       ItemStack is = (ItemStack)localIterator1.next();
/* 157 */       HashMap leftOver = p.getInventory().addItem(new ItemStack[] { is });
/* 158 */       localIterator2 = leftOver.values().iterator(); continue; ItemStack left = (ItemStack)localIterator2.next();
/* 159 */       remaining.add(left);
/*     */     }
/* 161 */     return remaining;
/*     */   }
/*     */ 
/*     */   public static ItemStack[] convertToBlocks(ItemStack[] startingItems) {
/* 165 */     int inventorySize = startingItems.length;
/* 166 */     ItemStack[] finalItems = Bukkit.createInventory(null, InventoryType.PLAYER).getContents();
/* 167 */     Map toConvert = new HashMap();
/*     */     ItemStack is;
/* 168 */     for (int i = 0; i < inventorySize; i++) {
/* 169 */       is = startingItems[i];
/* 170 */       if (is != null)
/*     */       {
/* 172 */         if (!blocksToAuto.containsKey(is.getType())) {
/* 173 */           finalItems[i] = is;
/*     */         } else {
/* 175 */           ArrayList intList = new ArrayList();
/* 176 */           if (toConvert.containsKey(is.getType())) {
/* 177 */             intList = (ArrayList)toConvert.get(is.getType());
/* 178 */             toConvert.remove(is.getType());
/*     */           }
/* 180 */           intList.add(Integer.valueOf(i));
/* 181 */           toConvert.put(is.getType(), intList);
/*     */         }
/*     */       }
/*     */     }
/* 184 */     for (Map.Entry entry : toConvert.entrySet()) {
/* 185 */       AutoBlockInfo info = (AutoBlockInfo)blocksToAuto.get(entry.getKey());
/* 186 */       Material type = info.getNewType();
/* 187 */       int requiredAmount = info.getRequiredAmount();
/* 188 */       int createdAmount = info.getCreatedAmount();
/*     */ 
/* 190 */       int leftOver = 0;
/* 191 */       for (Iterator localIterator1 = ((ArrayList)entry.getValue()).iterator(); localIterator1.hasNext(); ) { int i = ((Integer)localIterator1.next()).intValue();
/* 192 */         leftOver += startingItems[i].getAmount();
/*     */       }
/* 194 */       int created = 0;
/* 195 */       boolean changed = false;
/* 196 */       boolean good = false;
/* 197 */       while (!good)
/* 198 */         if (leftOver >= requiredAmount) {
/* 199 */           changed = true;
/* 200 */           leftOver -= requiredAmount;
/* 201 */           created += createdAmount;
/*     */         } else {
/* 203 */           good = true;
/*     */         }
/*     */       ItemStack createdIS;
/* 205 */       if (changed) {
/* 206 */         ArrayList toAdd = new ArrayList();
/* 207 */         createdIS = new ItemStack(type);
/* 208 */         ItemStack excessIS = new ItemStack((Material)entry.getKey());
/*     */ 
/* 210 */         good = false;
/* 211 */         while (!good) {
/* 212 */           if (created > 64) {
/* 213 */             created -= 64;
/* 214 */             createdIS.setAmount(64);
/*     */           } else {
/* 216 */             createdIS.setAmount(created);
/* 217 */             good = true;
/*     */           }
/* 219 */           if (createdIS.getAmount() <= 0)
/* 220 */             good = true;
/*     */           else {
/* 222 */             toAdd.add(createdIS);
/*     */           }
/*     */         }
/* 225 */         good = false;
/* 226 */         while (!good) {
/* 227 */           if (leftOver > 64) {
/* 228 */             leftOver -= 64;
/* 229 */             excessIS.setAmount(64);
/*     */           } else {
/* 231 */             excessIS.setAmount(leftOver);
/* 232 */             good = true;
/*     */           }
/* 234 */           if (excessIS.getAmount() <= 0)
/* 235 */             good = true;
/*     */           else {
/* 237 */             toAdd.add(excessIS);
/*     */           }
/*     */         }
/* 240 */         Inventory tempInv = Bukkit.createInventory(null, InventoryType.PLAYER);
/* 241 */         tempInv.setContents(finalItems);
/* 242 */         for (ItemStack is : toAdd) {
/* 243 */           tempInv.addItem(new ItemStack[] { is });
/*     */         }
/* 245 */         finalItems = tempInv.getContents();
/*     */       } else {
/* 247 */         for (createdIS = ((ArrayList)entry.getValue()).iterator(); createdIS.hasNext(); ) { int i = ((Integer)createdIS.next()).intValue();
/* 248 */           finalItems[i] = startingItems[i]; }
/*     */       }
/*     */     }
/* 251 */     return finalItems;
/*     */   }
/*     */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.Main
 * JD-Core Version:    0.6.2
 */