/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import org.bukkit.inventory.ItemStack;
/*    */ import org.bukkit.inventory.meta.ItemMeta;
/*    */ 
/*    */ public class NoPickup
/*    */ {
/*    */   public static boolean canPickup(ItemStack is)
/*    */   {
/* 11 */     if ((is == null) || (is.getItemMeta() == null) || (is.getItemMeta().getLore() == null) || 
/* 12 */       (!is.getItemMeta().getLore().contains("NOPICKUP111"))) {
/* 13 */       return true;
/*    */     }
/* 15 */     return false;
/*    */   }
/*    */ 
/*    */   public static ItemStack remove(ItemStack is) {
/* 19 */     if (!canPickup(is)) {
/* 20 */       ItemMeta im = is.getItemMeta();
/* 21 */       List lore = im.getLore();
/* 22 */       if (lore == null)
/* 23 */         lore = new ArrayList();
/* 24 */       lore.remove("NOPICKUP111");
/* 25 */       im.setLore(lore);
/* 26 */       is.setItemMeta(im);
/*    */     }
/* 28 */     return is;
/*    */   }
/*    */ 
/*    */   public static ItemStack add(ItemStack is) {
/* 32 */     if (canPickup(is)) {
/* 33 */       ItemMeta im = is.getItemMeta();
/* 34 */       List lore = im.getLore();
/* 35 */       if (lore == null)
/* 36 */         lore = new ArrayList();
/* 37 */       lore.add("NOPICKUP111");
/* 38 */       im.setLore(lore);
/* 39 */       is.setItemMeta(im);
/*    */     }
/* 41 */     return is;
/*    */   }
/*    */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.NoPickup
 * JD-Core Version:    0.6.2
 */