package me.lukasabc123abc.messager;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Msg extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Msg plugin;
	private String prefix = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine" + ChatColor.GRAY + ":";
	private String prefix2 = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine";
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
	}
		private Map<String, String> messagers = new HashMap<String, String>();
	    @Override
	    public boolean onCommand(CommandSender sender, Command cmd,
	            String commandLabel, String[] args) {
	 
	        if (!(sender instanceof Player)) {
	            sender.sendMessage(ChatColor.RED + "Only Players can use this command.");
	            return true;
	        }
	 
	 
	        Player player = (Player) sender;
	        if (cmd.getName().equalsIgnoreCase("pm")) {
	            if(args.length < 2){
	                sender.sendMessage(ChatColor.RED + "The command requires 2 Parametes: target and message.");
	                return true;
	            }
	 
	            if (sender.hasPermission("yg.pm")) {
	                Player target = (Bukkit.getServer().getPlayer(args[0]));
	                if (target == null) {
	                    sender.sendMessage(args[0] + " is not online!");
	                    return true;
	                } else {
	                    String message = "";
	                    for (int i = 1; i < args.length; i++) {
	                        message = message + args[i] + ' ';
	                    }
	 
	                    player.sendMessage("�6[�7me �a-> �7" + target.getName() + "�6] �7" + message);
	                    target.sendMessage("�6[�7" + player.getName() + " �a -> �7me�6] �7" + message);
	                    messagers.put(player.getName(), target.getName());
	                }
	            }else{
	                sender.sendMessage(ChatColor.RED + "You have no Permission for this command.");
	            }
	 
	            return true;
	 
	        }
	 
	        if (cmd.getName().equalsIgnoreCase("r")) {
	            if(args.length == 0){
	                sender.sendMessage(ChatColor.RED + "You need to give a message.");
	                return true;
	            }
	 
	            if (sender.hasPermission("yg.reply")) {
	                if (!messagers.containsKey(player.getName())) {
	                    player.sendMessage("You have no one to resent to.");
	                    return true;
	                }
	 
	                Player target = Bukkit.getPlayer(messagers.get(player.getName()));
	                if(target == null || !target.isOnline()){
	                    sender.sendMessage(ChatColor.RED + messagers.get(player.getName())
	                            + "is not online.");
	                    return true;
	                }
	 
	                String message = "";
	                for (int i = 0; i < args.length; i++) {
	                    message = message + args[i] + ' ';
	                }
	 
	                player.sendMessage("�6[�7me �a-> �7" + target.getName() + "�6] �7" + message);
	                target.sendMessage("�6[�7" + player.getName() + " �a -> �7me�6] �7" + message);
	            }
	        }
	 
	        return false;
	    }
	}
		
