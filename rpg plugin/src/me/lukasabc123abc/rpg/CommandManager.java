package me.lukasabc123abc.rpg;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class CommandManager implements CommandExecutor, Listener{
	
	public String prefix = ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "RPG" + ChatColor.BLACK + "] ";
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("coins")) { // If the player typed /basic then do the following...
			
			if(sender instanceof Player){
			Player player = (Player) sender;
			FileConfiguration config = null;
	        File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+player.getName()+".yml");
	        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
	        player.sendMessage(ChatColor.GOLD + "Coins" + ChatColor.GRAY + ChatColor.BOLD + "> " + config1.getLong("Stats.coins"));

			}else{
				sender.sendMessage("You have to be a player to see your coins!");
				
			}
			return true;
		}else if(label.equalsIgnoreCase("stats")){
            if(!sender.hasPermission("lukasabc123abc.stats")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if(target != null) {
                //load player config
		        File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+ target.getName()+".yml");
		        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
		        //load levels file
		        File file1 = new File("plugins"+File.separator+"rpg"+File.separator+"levels.yml");
		        FileConfiguration config2 = YamlConfiguration.loadConfiguration(file1);
				
				sender.sendMessage(prefix + ChatColor.GREEN + " Stats of " + ChatColor.BLUE + target.getName());
				sender.sendMessage(ChatColor.RED + "Health: " + config1.getDouble("Stats.health"));
				sender.sendMessage(ChatColor.RED + "Level: " + config1.getInt("Stats.level"));
				sender.sendMessage(ChatColor.RED + "Exp: " + config1.get("Stats.exp") + " / " + config2.get("exp.level" + config1.getInt("Stats.level")));
				sender.sendMessage(ChatColor.RED + "Exp%: " + (config1.getInt("Stats.exp"))  /  (config2.getInt("exp.level" + config1.getInt("Stats.level"))) * 100 + "%");
                }else {
                    sender.sendMessage(ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "RPG" + ChatColor.BLACK + "] " + ChatColor.RED + "Player offline!");
                    return true;
                }
            }else{
            	File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+ sender.getName()+".yml");
		        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
		        //load levels file
		        File file1 = new File("plugins"+File.separator+"rpg"+File.separator+"levels.yml");
		        FileConfiguration config2 = YamlConfiguration.loadConfiguration(file1);
				
				sender.sendMessage(prefix + ChatColor.GREEN + " Stats of " + ChatColor.BLUE + sender.getName());
				sender.sendMessage(ChatColor.RED + "Health: " + config1.getDouble("Stats.health"));
				sender.sendMessage(ChatColor.RED + "Level: " + config1.getInt("Stats.level"));
				sender.sendMessage(ChatColor.RED + "Exp: " + config1.get("Stats.exp") + " / " + config2.get("exp.level" + config1.getInt("Stats.level")));
            	
            }
        }else if(label.equalsIgnoreCase("utiladdexp")){
            if(!sender.hasPermission("lukasabc123abc.addexp")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
            int amount = Integer.parseInt(args[1]);
			//if the target is online
            if(target instanceof Player) {
                //passed #1
            	//good add the exp here	
                    File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+target.getName()+".yml");
                    FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
                    target.sendMessage(prefix + ChatColor.BLUE + "You received " + amount + " Exp!");
                    config1.set("Stats.exp", config1.getInt("Stats.exp") + amount);
                    try {
						config1.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
            }else{
            	sender.sendMessage(prefix + ChatColor.RED + "Player offline!");
            	
            }}else {
                sender.sendMessage(prefix + ChatColor.RED + "invalid usage. use /utiladdexp {player} {amount}");
                return true;
            }
        }
        else if(label.equalsIgnoreCase("sendkilled")){
            if(!sender.hasPermission("lukasabc123abc.sendkilled")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
            String boss = args[1];
            
            Bukkit.broadcastMessage(ChatColor.GOLD + "-== " + target.getName() + "killed The boss " + boss + "==-");
            
            }
        }else if(label.equalsIgnoreCase("utilsetlevel")){
            if(!sender.hasPermission("lukasabc123abc.setlevel")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
            int amount = Integer.parseInt(args[1]);
			//if the target is online
            if(target instanceof Player) {
                //passed #1
            	//good add the exp here	
                    File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+target.getName()+".yml");
                    FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
                    target.sendMessage(prefix + ChatColor.BLUE + "Your level has been set to  " + amount + "!");
                    config1.set("Stats.level", amount);
                    try {
						config1.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),"pex user " + target.getName() + " suffix " + config1.getInt("Stats.level"));
                    target.setLevel(config1.getInt("Stats.level"));
                    target.setExp(0);
                }else{
                	sender.sendMessage(prefix + ChatColor.RED + "Player offline!");
                	
                }}else {
                    sender.sendMessage(prefix + ChatColor.RED + "invalid usage. use /utilsetlevel {player} {amount}");
                    return true;
                }
        }else if(label.equalsIgnoreCase("utilsetcoins")){
            if(!sender.hasPermission("lukasabc123abc.setcoins")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
            int amount = Integer.parseInt(args[1]);
			//if the target is online
            if(target instanceof Player) {
                //passed #1
            	//good add the exp here	
                    File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+target.getName()+".yml");
                    FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
                    target.sendMessage(prefix + ChatColor.BLUE + "your coins has been set to  " + amount + "!");
                    config1.set("Stats.coins", amount);
                    try {
						config1.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
            }else{
            	sender.sendMessage(prefix + ChatColor.RED + "Player offline!");
            	
            }}else {
                sender.sendMessage(prefix + ChatColor.RED + "invalid usage. use /utilsetcoins {player} {amount}");
                return true;
            }
        }else if(label.equalsIgnoreCase("utiladdcoins")){
            if(!sender.hasPermission("lukasabc123abc.addcoins")) {
                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
                return true;
            }
            if(args.length > 0){
            @SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(args[0]);
            int amount = Integer.parseInt(args[1]);
			//if the target is online
            if(target instanceof Player) {
                //passed #1
            	//good add the exp here	
                    File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+target.getName()+".yml");
                    FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
                    target.sendMessage(prefix + ChatColor.BLUE + "You received " + amount + " Coins!");
                    config1.set("Stats.coins", config1.getInt("Stats.coins") + amount);
                    try {
						config1.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
            }else{
            	sender.sendMessage(prefix + ChatColor.RED + "Player offline!");
            	
            }}else {
                sender.sendMessage(prefix + ChatColor.RED + "invalid usage. use /utiladdcoins {player} {amount}");
                return true;
            }
        }
		return false;

}
}
