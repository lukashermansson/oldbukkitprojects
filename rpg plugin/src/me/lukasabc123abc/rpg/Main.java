package me.lukasabc123abc.rpg;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Skull;
import org.bukkit.command.Command;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	static Plugin plugin;
	public String prefix = ChatColor.BLACK + "[" + ChatColor.DARK_RED + ChatColor.BOLD + "RPG" + ChatColor.BLACK + "] ";
	@Override
	public void onDisable() 
	{
		plugin = null;
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
			
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {

		registerEvents(this, new CommandManager());
		getCommand("coins").setExecutor(new CommandManager());
		getCommand("stats").setExecutor(new CommandManager());
		getCommand("utiladdexp").setExecutor(new CommandManager());
		getCommand("sendkilled").setExecutor(new CommandManager());
		getCommand("utilsetlevel").setExecutor(new CommandManager());
		getCommand("utiladdcoins").setExecutor(new CommandManager());
		getCommand("utilsetcoins").setExecutor(new CommandManager());
		plugin = this;
		checklevel();
		getServer().getPluginManager().registerEvents(this, this);
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		// create a config
		FileConfiguration config = null;
		
        
        File file = new File("plugins"+File.separator+"rpg"+File.separator+"config.yml");
        if(!file.exists()){
            System.out.println("File Created: config.yml");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not create a new config file");
                    e.printStackTrace();
                }
                //write to file
                config = YamlConfiguration.loadConfiguration(file);
                config.set("options.maxlevel", 1000);
               
                try {
                    config.save(file);
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not save the config file for the fist time");
                    e.printStackTrace();
                }
        }
		// create a config
		FileConfiguration config1 = null;
		
        
        File file1 = new File("plugins"+File.separator+"rpg"+File.separator+"levels.yml");
        if(!file1.exists()){
            System.out.println("File Created: levels.yml");
                try {
                    file1.createNewFile();
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not create a new levels file");
                    e.printStackTrace();
                }
                //write to file
                config1 = YamlConfiguration.loadConfiguration(file1);
                for(int x = 1; x < 99; x = x+1) {
                config1.set("exp.level"+ x, 50);
                }
                try {
                    config1.save(file1);
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not save the levels file for the fist time");
                    e.printStackTrace();
                }
        }
	
	plugin.getServer().getScheduler().scheduleAsyncRepeatingTask(plugin, new Runnable() {
		 
		  public void run() {
		      checklevel();
		  }
		}, 0L, 20L);
	}
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }
	
	@EventHandler
	public void onPlayerjoin(PlayerJoinEvent event){
		FileConfiguration config = null;
		event.setJoinMessage(ChatColor.GREEN + event.getPlayer().getName() + " Joined!");
		setstuff(event.getPlayer());
		//skriv en fil för spelaren
		
        File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator + event.getPlayer().getName() + ".yml");
        if(!file.exists()){
            System.out.println("File Created: "+ event.getPlayer().getName() + ".yml");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not create a new file for " + event.getPlayer().getName());
                    e.printStackTrace();
                }
                config = YamlConfiguration.loadConfiguration(file);
                config.set("Player.Name", event.getPlayer());
                config.set("Stats.level", 1);
                config.set("Stats.exp", 0);
                config.set("Stats.coins", 50);
                config.set("Stats.health", 200.0);
               
                try {
                    config.save(file);
                } catch (IOException e) {
                	Bukkit.getServer().getLogger().warning("[RPG]" + " coud not save a new file for " + event.getPlayer().getName());
                	System.err.println("coud not save a newly created file");
                    e.printStackTrace();
                }
        }
        File playerfile = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+event.getPlayer().getName()+".yml");
        FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(file);
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),"pex user " + event.getPlayer().getName() + " suffix " + playerconfig.getInt("Stats.level"));
	}
	
	@SuppressWarnings("deprecation")
	public void setstuff(Player player){
		FileConfiguration config = null;
        File file = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+player.getName()+".yml");
        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
        Bukkit.getServer().getLogger().warning("[RPG]" + " en person joinade presis " + " hans level är " + config1.get("Stats.level"));
		player.setLevel(config1.getInt("Stats.level"));
		player.setExp(config1.getInt("Stats.exp"));
		
	}
	public void checklevel(){

	for (Player player : this.plugin.getServer().getOnlinePlayers()){
	
	
	    File playerfile = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+ player.getName()+".yml");
	    FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
	    
	    File levelfile = new File("plugins"+File.separator+"rpg"+File.separator+"levels.yml");
        FileConfiguration levelconfig = YamlConfiguration.loadConfiguration(levelfile);
		
		if(playerconfig.getInt("Stats.exp") >= levelconfig.getInt("exp.level" + playerconfig.getInt("Stats.level"))){
			playerconfig.set("Stats.level", playerconfig.getInt("Stats.level") + 1);
			player.setLevel(playerconfig.getInt("Stats.level"));
			player.setExp(0);
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),"pex user " + player.getName() + " suffix " + playerconfig.getInt("Stats.level"));
			try {
				playerconfig.save(playerfile);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			int levelexp = levelconfig.getInt("exp.level" + playerconfig.getInt("Stats.level"));
			playerconfig.set("Stats.exp", playerconfig.getInt("Stats.exp") - levelexp);
			try {
				playerconfig.save(playerfile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			player.sendMessage(prefix + ChatColor.GOLD + ChatColor.BOLD + "You leveled to level: " + ChatColor.RED + playerconfig.getInt("Stats.level"));
		
	}
	}
	}
	@EventHandler
	public void onPlayerlevae(PlayerQuitEvent event){
		event.setQuitMessage(ChatColor.RED  + event.getPlayer().getName() + " left!");
		
	}
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent event){
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
		            Player player = event.getPlayer();
		            BlockState block = event.getClickedBlock().getState();
		            if (block instanceof Skull) {
		                Skull skull = (Skull) block;
		                String owner = skull.getOwner();
		                if(owner.equals("egg")){
		                	event.getClickedBlock().setType(Material.AIR);
		                	player.sendMessage(prefix + ChatColor.GOLD + ChatColor.BOLD + "You found an ester egg");
		            	    File playerfile = new File("plugins"+File.separator+"rpg"+File.separator+"players"+File.separator+ player.getName()+".yml");
		            	    FileConfiguration playerconfig = YamlConfiguration.loadConfiguration(playerfile);
		            	    playerconfig.set("Stats.eggs", playerconfig.getInt("Stats.eggs") + 1);
		            	    try {
								playerconfig.save(playerfile);
							} catch (IOException e) {
								e.printStackTrace();
							}
		                }
		            }
		  }
		 
		}
}
