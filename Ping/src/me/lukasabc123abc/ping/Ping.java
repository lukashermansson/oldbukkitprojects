package me.lukasabc123abc.ping;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Ping extends JavaPlugin implements Listener {
	public final Logger logger = Logger.getLogger("minecraft");
	public static Ping plugin;
	boolean Muted;
	String prefix = ChatColor.translateAlternateColorCodes('&', "&8[&bMaster&cMine&8]");
	@Override
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	
	@Override
	public void onEnable() {
		Muted = false;
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
		getServer().getPluginManager().registerEvents(this, this);
	}

	public int getPing(Player player) {
	    return ((CraftPlayer) player).getHandle().ping;
	}
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        if(commandLabel.equalsIgnoreCase("ping")){
            	if(args.length <= 0){
            		if(sender.hasPermission("ping.pingself")){
            		if(sender instanceof Player){
            			if(getPing((Player) sender) < 120){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: "  + ChatColor.GREEN + "["+ ChatColor.GREEN + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}else if(getPing((Player) sender) < 200){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: " + ChatColor.GREEN + "[" + ChatColor.RED + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}
            	else{
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: "  + ChatColor.GREEN + "["+ ChatColor.DARK_RED + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}
            		}else{
            			sender.sendMessage(prefix + ChatColor.RED + "You must be a player to ping yourself you can use /ping {player} to ping an online player");
            		}
            	}else{
            		sender.sendMessage(prefix + ChatColor.RED + "You do not have permission");
            	}
            	}else if(args.length == 1){
            		if(sender.hasPermission("ping.pingothers")){
            		
            		if(getServer().getPlayer(args[0]) != null){
            			Player target = getServer().getPlayer(args[0]);
            			if(getPing((Player) target) < 120){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "[" + ChatColor.GREEN + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}else if(getPing((Player) target) < 200){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "["+ ChatColor.RED + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}
            			else{
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "[" + ChatColor.DARK_RED + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}
            		}else{
            			sender.sendMessage(prefix + ChatColor.RED + "That player is offline");
            		}
            	}else{
            		sender.sendMessage(ChatColor.RED + "You do not have permission");
            	}
            	}else{
            		sender.sendMessage(prefix + ChatColor.RED + "Invalid argument use /ping or /ping {player}");
            	}
            	
            	
            }
     
		return false;
}
	
}	
