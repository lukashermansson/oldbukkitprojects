/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import java.util.List;
/*    */ import org.bukkit.configuration.ConfigurationSection;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.inventory.ItemStack;
/*    */ 
/*    */ public class SuperYaml
/*    */ {
/*    */   private String fileLocation;
/*    */   public YamlConfiguration config;
/*    */ 
/*    */   public SuperYaml(String fileLocation)
/*    */   {
/* 14 */     this.fileLocation = fileLocation;
/* 15 */     reload();
/*    */   }
/*    */ 
/*    */   public void reload() {
/* 19 */     this.config = Config.Load(this.fileLocation);
/*    */   }
/*    */ 
/*    */   public void save() {
/* 23 */     Config.Save(this.config, this.fileLocation);
/*    */   }
/*    */ 
/*    */   public void set(String path, Object value) {
/* 27 */     this.config.set(path, value);
/*    */   }
/*    */ 
/*    */   public Object get(String path) {
/* 31 */     return this.config.get(path);
/*    */   }
/*    */ 
/*    */   public int getInt(String path) {
/* 35 */     return this.config.getInt(path);
/*    */   }
/*    */ 
/*    */   public Boolean getBoolean(String path) {
/* 39 */     return Boolean.valueOf(this.config.getBoolean(path));
/*    */   }
/*    */ 
/*    */   public double getDouble(String path) {
/* 43 */     return this.config.getDouble(path);
/*    */   }
/*    */ 
/*    */   public ItemStack getItemStack(String path) {
/* 47 */     return this.config.getItemStack(path);
/*    */   }
/*    */ 
/*    */   public ConfigurationSection getConfigurationSection(String path) {
/* 51 */     return this.config.getConfigurationSection(path);
/*    */   }
/*    */ 
/*    */   public String getString(String path) {
/* 55 */     return this.config.getString(path);
/*    */   }
/*    */ 
/*    */   public List<String> getStringList(String path) {
/* 59 */     return this.config.getStringList(path);
/*    */   }
/*    */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.SuperYaml
 * JD-Core Version:    0.6.2
 */