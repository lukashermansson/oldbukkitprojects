/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import org.bukkit.ChatColor;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ import org.bukkit.configuration.file.YamlConfigurationOptions;
/*    */ 
/*    */ public class Messages
/*    */ {
/*    */   public static String COMMAND_BLOCK;
/*    */   public static String COMMAND_BLOCK_FAIL;
/*    */   public static String COMMAND_SMELT;
/*    */   public static String COMMAND_SMELT_FAIL;
/*    */   public static String FULL_INVENTORY;
/*    */   public static String PREFIX;
/*    */ 
/*    */   public static void setUp()
/*    */   {
/* 29 */     COMMAND_BLOCK = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine!" +
        ChatColor.GREEN + " I made your ingots back to blocks";
/* 30 */     COMMAND_BLOCK_FAIL = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine!" +
        ChatColor.DARK_RED + " you do not have enough ingots to make blocks from";
/* 31 */     COMMAND_SMELT = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine!" +
        ChatColor.GREEN + " I smelted all your blocks for you!";
/* 32 */     COMMAND_SMELT_FAIL = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine!" +
        ChatColor.DARK_RED + "Your Inventory has nothing that can be smelted!";
/* 33 */     FULL_INVENTORY = ChatColor.AQUA + "Master" + ChatColor.RED + "Mine!" +
             ChatColor.DARK_RED + " Your inventory is Full!!";
/*    */   }
/*    */ }
