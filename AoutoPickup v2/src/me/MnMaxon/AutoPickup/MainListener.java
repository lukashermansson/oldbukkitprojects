/*     */ package me.MnMaxon.AutoPickup;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Random;
/*     */ import org.bukkit.Location;
/*     */ import org.bukkit.Material;
/*     */ import org.bukkit.World;
/*     */ import org.bukkit.block.Block;
/*     */ import org.bukkit.enchantments.Enchantment;
/*     */ import org.bukkit.entity.Entity;
/*     */ import org.bukkit.entity.Item;
/*     */ import org.bukkit.entity.LivingEntity;
/*     */ import org.bukkit.entity.Player;
/*     */ import org.bukkit.event.EventHandler;
/*     */ import org.bukkit.event.EventPriority;
/*     */ import org.bukkit.event.Listener;
/*     */ import org.bukkit.event.block.BlockBreakEvent;
/*     */ import org.bukkit.event.entity.EntityDeathEvent;
/*     */ import org.bukkit.event.entity.ItemSpawnEvent;
/*     */ import org.bukkit.event.player.PlayerDropItemEvent;
/*     */ import org.bukkit.inventory.ItemStack;
/*     */ import org.bukkit.inventory.PlayerInventory;
/*     */ 
/*     */ public class MainListener
/*     */   implements Listener
/*     */ {
/*     */   @EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
/*     */   public void onBreak(BlockBreakEvent e)
/*     */   {
/*  24 */     Player p = e.getPlayer();
/*  25 */     if ((p.hasPermission(Permissions.INFINITE_PICK)) && (p.getItemInHand().getType().name().contains("PICK"))) {
/*  26 */       p.getItemInHand().setDurability((short)-1);
/*     */     }
/*  28 */     if ((p.hasPermission(Permissions.AUTO_SMELT)) && 
/*  29 */       (p.getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)))
/*     */     {
/*     */       Material material;
/*  31 */       if (e.getBlock().getType().equals(Material.IRON_ORE)) {
/*  32 */         material = Material.IRON_INGOT;
/*     */       }
/*     */       else
/*     */       {
/*     */         Material material1;
/*  33 */         if (e.getBlock().getType().equals(Material.GOLD_ORE))
/*  34 */           material1 = Material.GOLD_INGOT;
/*     */         else
/*     */           return;
/*     */       }
/*     */       Material material1 = null;
/*  37 */       int i = p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
/*  38 */       int j = new Random().nextInt(i);
/*  39 */       ItemStack finalItem = new ItemStack(material1);
/*  40 */       finalItem.setAmount(j);
/*  41 */       Location loc = e.getBlock().getLocation().add(0.5D, 0.5D, 0.5D);
/*  42 */       loc.getWorld().dropItemNaturally(loc, finalItem);
/*     */     }
/*  44 */     p.giveExp(e.getExpToDrop());
/*  45 */     e.setExpToDrop(0);
/*     */   }
/*     */ 
/*     */   @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
/*     */   public void onItemSpawn(ItemSpawnEvent e)
/*     */   {
/*  51 */     if (!NoPickup.canPickup(e.getEntity().getItemStack())) {
/*  52 */       e.getEntity().setItemStack(NoPickup.remove(e.getEntity().getItemStack()));
/*  53 */       return;
/*     */     }
/*     */ 
/*  56 */     Player p = null;
/*  57 */     if (p == null) {
/*  58 */       p = getNearby(e.getEntity(), 0.5D);
/*  59 */       if (p == null) {
/*  60 */         p = getNearby(e.getEntity(), 1.0D);
/*  61 */         if (p == null) {
/*  62 */           p = getNearby(e.getEntity(), 2.0D);
/*  63 */           if (p == null) {
/*  64 */             p = getNearby(e.getEntity(), 3.0D);
/*  65 */             if (p == null) {
/*  66 */               p = getNearby(e.getEntity(), 4.0D);
/*  67 */               if (p == null) {
/*  68 */                 p = getNearby(e.getEntity(), 5.0D);
/*  69 */                 if (p == null) {
/*  70 */                   p = getNearby(e.getEntity(), 6.0D);
/*  71 */                   if (p == null) {
/*  72 */                     return;
/*     */                   }
/*     */                 }
/*     */               }
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*  81 */     ArrayList<ItemStack> finalItems = new ArrayList();
/*  82 */     finalItems.add(e.getEntity().getItemStack());
/*     */ 
/*  84 */     if (p.hasPermission(Permissions.AUTO_SMELT)) {
/*  85 */       for (ItemStack is : finalItems) {
/*  86 */         if (Main.blocksToSmelt.containsKey(is.getType()))
/*  87 */           is.setType(((AutoBlockInfo)Main.blocksToSmelt.get(is.getType())).getNewType());
/*     */       }
/*     */     }
/*  90 */     finalItems = Main.addToInventory(p, finalItems);
/*     */ 
/*  92 */     if (p.hasPermission(Permissions.AUTO_BLOCK)) {
/*  93 */       ItemStack[] newInvCont = Main.convertToBlocks(p.getInventory().getContents());
/*  94 */       if (!p.getInventory().getContents().equals(newInvCont)) {
/*  95 */         p.getInventory().setContents(newInvCont);
/*  96 */         p.updateInventory();
/*     */       }
/*     */     }
/*     */ 
/* 100 */     if (!finalItems.isEmpty())
/* 101 */       p.sendMessage(Messages.FULL_INVENTORY);
/* 102 */     e.getEntity().remove();
/*     */   }
/*     */ 
/*     */   private Player getNearby(Item item, double range) {
/* 106 */     for (Entity e : item.getNearbyEntities(range, range, range))
/* 107 */       if ((e instanceof Player))
/* 108 */         return (Player)e;
/* 109 */     return null;
/*     */   }
/*     */ 
/*     */   @EventHandler(priority=EventPriority.HIGHEST)
/*     */   public void onEntityDeath(EntityDeathEvent e) {
/* 114 */     ArrayList<ItemStack> drops = new ArrayList(e.getDrops());
/* 115 */     e.getDrops().clear();
/* 116 */     Player killer = e.getEntity().getKiller();
/* 117 */     if ((e.getEntity() instanceof Player))
/* 118 */       for (ItemStack is : drops) {
/* 119 */         is = NoPickup.add(is);
/* 120 */         e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), is);
/*     */       }
/* 122 */     else if ((killer != null) && (killer.isValid())) {
/* 123 */       for (ItemStack is : drops)
/* 124 */         killer.getWorld().dropItemNaturally(killer.getLocation(), is);
/*     */     }
/* 126 */     if ((killer != null) && (!(e.getEntity() instanceof Player))) {
/* 127 */       killer.giveExp(e.getDroppedExp());
/* 128 */       e.setDroppedExp(0);
/*     */     }
/*     */   }
/*     */ 
/*     */   @EventHandler(priority=EventPriority.HIGHEST)
/*     */   public void onItemDrop(PlayerDropItemEvent e)
/*     */   {
/* 135 */     if (!e.isCancelled())
/* 136 */       e.getItemDrop().setItemStack(NoPickup.add(e.getItemDrop().getItemStack()));
/*     */   }
/*     */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.MainListener
 * JD-Core Version:    0.6.2
 */