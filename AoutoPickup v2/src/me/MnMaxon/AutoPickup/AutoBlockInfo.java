/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import org.bukkit.Material;
/*    */ 
/*    */ public class AutoBlockInfo
/*    */ {
/*    */   private int requiredAmount;
/*    */   private Material newType;
/*    */   private int createdAmount;
/*    */ 
/*    */   public AutoBlockInfo(int requiredAmount, Material newType, int createdAmount)
/*    */   {
/* 11 */     setRequiredAmount(requiredAmount);
/* 12 */     setNewType(newType);
/* 13 */     setCreatedAmount(createdAmount);
/*    */   }
/*    */ 
/*    */   public int getCreatedAmount() {
/* 17 */     return this.createdAmount;
/*    */   }
/*    */ 
/*    */   private void setCreatedAmount(int createdAmount) {
/* 21 */     this.createdAmount = createdAmount;
/*    */   }
/*    */ 
/*    */   public Material getNewType() {
/* 25 */     return this.newType;
/*    */   }
/*    */ 
/*    */   private void setNewType(Material newType) {
/* 29 */     this.newType = newType;
/*    */   }
/*    */ 
/*    */   public int getRequiredAmount() {
/* 33 */     return this.requiredAmount;
/*    */   }
/*    */ 
/*    */   private void setRequiredAmount(int requiredAmount) {
/* 37 */     this.requiredAmount = requiredAmount;
/*    */   }
/*    */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.AutoBlockInfo
 * JD-Core Version:    0.6.2
 */