/*    */ package me.MnMaxon.AutoPickup;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.io.IOException;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ import org.bukkit.Bukkit;
/*    */ import org.bukkit.Server;
/*    */ import org.bukkit.configuration.InvalidConfigurationException;
/*    */ import org.bukkit.configuration.file.YamlConfiguration;
/*    */ 
/*    */ public class Config
/*    */ {
/*    */   public static YamlConfiguration Load(String FileLocation)
/*    */   {
/* 14 */     if (!new File(Main.dataFolder).exists())
/* 15 */       new File(Main.dataFolder).mkdirs();
/* 16 */     if (!new File(FileLocation).exists())
/*    */       try {
/* 18 */         new File(FileLocation).createNewFile();
/* 19 */         Bukkit.getServer().getLogger().log(Level.INFO, "New Config Created at: " + FileLocation);
/*    */       } catch (IOException e1) {
/* 21 */         e1.printStackTrace();
/*    */       }
/* 23 */     YamlConfiguration cfg = new YamlConfiguration();
/*    */     try {
/* 25 */       cfg.load(new File(FileLocation));
/*    */     } catch (IOException|InvalidConfigurationException e) {
/* 27 */       e.printStackTrace();
/*    */     }
/* 29 */     return cfg;
/*    */   }
/*    */ 
/*    */   public static void Save(YamlConfiguration cfg, String FileLocation) {
/*    */     try {
/* 34 */       cfg.save(new File(FileLocation));
/*    */     } catch (IOException e) {
/* 36 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\lukas hermansson\Downloads\AutoPickup_1.9.jar
 * Qualified Name:     me.MnMaxon.AutoPickup.Config
 * JD-Core Version:    0.6.2
 */