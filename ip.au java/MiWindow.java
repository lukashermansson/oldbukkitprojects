

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeListener;

public class MiWindow extends Canvas implements MouseListener, MouseMotionListener {
	
	static String ljud, ljud2;
	int x, y;
	int tr�ffar = 0;
	int missar = 0;
	String str, str2, str3;
	int i;
	DecimalFormat df = new DecimalFormat("##.##");
	Image bild;
	static JFrame frame;
	Canvas canavas;
	String version = "1.02";
	public static JFrame getFrame(){
		return frame;
	}
	public MiWindow(){
		frame = new JFrame("IPAU");
		JTabbedPane jpane = new JTabbedPane();
		JPanel Changelog = new JPanel();
		JTextArea textField = new JTextArea(20, 30);
		LoadChangelog(textField);
		textField.setEditable(false);
		JScrollPane scroll = new JScrollPane (textField, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		Changelog.add(scroll);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		ljud = "ip.wav";
		ljud2 = "pew.wav";
		try {
			bild = ImageIO.read(getClass().getResource("pacman.gif"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		canavas = this;
		canavas.addMouseListener(this);
		canavas.addMouseMotionListener(this);
		jpane.add("Game", canavas);
		jpane.add("ChangeLog", Changelog);
		frame.add(jpane);
		
		frame.setVisible(true);
	}
	private void LoadChangelog(JTextArea textField) {
		FileReader fileReader;
		try {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("c.txt")));
        while((bufferedReader.readLine()) != null) {
        	String line = null;
      	  line = bufferedReader.readLine();
      	  textField.append(line + "\n");
        }
        bufferedReader.close();         
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	Image backBuffer;
	Graphics bBG;
	int prevx, prevy;
	public void paint(Graphics g){
		 if( backBuffer == null || (!(prevx == getWidth())) || !(prevy == getHeight()))
	      {
	          backBuffer = createImage( getWidth(), getHeight() );
	          bBG = backBuffer.getGraphics();
	      }
		 prevx = getWidth();
		 prevy = getHeight();
		 bBG.setColor(Color.white);
		 bBG.fillRect(0, 0, canavas.getWidth(), canavas.getHeight());
		 bBG.setColor(Color.black);
		 bBG.drawImage(bild, x, y, canavas.getWidth()/10, canavas.getHeight()/10, this);
		 bBG.drawString("version " + version, 5, 10);
		str = tr�ffar + " Tr�ffar";
		int textwidth = bBG.getFontMetrics().stringWidth(str);
		bBG.drawString(str, canavas.getWidth() - textwidth, canavas.getHeight() - 20);
		str2 = missar + " Missar";
		int textwidth2 = bBG.getFontMetrics().stringWidth(str2);
		bBG.drawString(str2, canavas.getWidth() - textwidth2, canavas.getHeight() - 30);
		if(missar < 1){
			str3 = 0 + "% Tr�ffs�kerhet";
		}else{
			str3 = df.format(((float)tr�ffar/missar) * 100) + "% Tr�ffs�kerhet";
		}
		
		int textwidth3 = bBG.getFontMetrics().stringWidth(str3);
		bBG.drawString(str3, canavas.getWidth() - textwidth3, canavas.getHeight() - 10);
		g.drawImage( backBuffer, 0, 0, this );
	}
	public void update(Graphics g){
		g.clearRect(0, 0, canavas.getWidth(), canavas.getHeight());
		x = (int)(Math.random()*(canavas.getWidth() - canavas.getWidth()/10));
		y = (int)(Math.random()*(canavas.getHeight() - canavas.getHeight()/10));
		paint(g);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		//fixade tr�ffregistreringen.
		if((e.getX() > x && e.getX() < x + this.getWidth()/10) && (e.getY() > y && e.getY() < y + this.getHeight()/10)){
			PlaySound(ljud2);
			tr�ffar++;
		}else{
			PlaySound(ljud);
			missar++;
			
		}
		canavas.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		canavas.repaint();
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		canavas.repaint();
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		canavas.repaint();
	}
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		if((e.getX() % 3 == 0) && (e.getY() % 3 == 0)){
			canavas.repaint();
		}
		
	}
	public void PlaySound(String sound){
		try {
			final Clip clip = AudioSystem.getClip();
			InputStream input = new BufferedInputStream(getClass().getResourceAsStream(sound));
			clip.open(AudioSystem.getAudioInputStream(input));	
			clip.addLineListener(new LineListener()
		        {
		            @Override
		            public void update(LineEvent event)
		            {
		                if (event.getType() == LineEvent.Type.STOP)
		                	clip.close();
		            }
		        });
			clip.start();
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
