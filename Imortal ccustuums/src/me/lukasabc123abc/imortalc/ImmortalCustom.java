package me.lukasabc123abc.imortalc;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ImmortalCustom extends JavaPlugin
  implements Listener
{
  HashMap<ItemStack, Material> materials = new HashMap();
  HashMap<Player, Integer> counts = new HashMap();
  Plugin WE = null;

  public void onEnable() {
    getConfig().options().copyDefaults(true);
    saveDefaultConfig();

    this.materials.put(new ItemStack(Material.DIAMOND, 9), Material.DIAMOND_BLOCK);
    this.materials.put(new ItemStack(Material.EMERALD, 9), Material.EMERALD_BLOCK);
    this.materials.put(new ItemStack(Material.GOLD_INGOT, 9), Material.GOLD_BLOCK);
    this.materials.put(new ItemStack(Material.IRON_INGOT, 9), Material.IRON_BLOCK);
    this.materials.put(new ItemStack(Material.QUARTZ, 9), Material.QUARTZ_BLOCK);
    this.materials.put(new ItemStack(Material.REDSTONE, 9), Material.REDSTONE_BLOCK);
    this.materials.put(new ItemStack(Material.COAL, 9), Material.COAL_BLOCK);
    this.materials.put(new ItemStack(Material.INK_SACK, 9, (short)4), Material.LAPIS_BLOCK);
    this.WE = Bukkit.getPluginManager().getPlugin("WorldEdit");

    Bukkit.getPluginManager().registerEvents(this, this);
  }

  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
    if (args.length == 0) {
      if ((sender.hasPermission("ImmortalCustom.version")) || (sender.hasPermission("ic.version"))) {
        sender.sendMessage(ChatColor.LIGHT_PURPLE + ChatColor.STRIKETHROUGH + "=============" + ChatColor.GOLD + "[ " + ChatColor.DARK_AQUA + "ImmortalCustom" + ChatColor.GOLD + " ]" + ChatColor.LIGHT_PURPLE + ChatColor.STRIKETHROUGH + "=============");
        sender.sendMessage(ChatColor.GOLD + "Plugin: " + ChatColor.YELLOW + "ImmortalCustom");
        sender.sendMessage(ChatColor.GOLD + "Author: " + ChatColor.YELLOW + "au2001");
        sender.sendMessage(ChatColor.GOLD + "Version: " + ChatColor.YELLOW + getDescription().getVersion());
        sender.sendMessage(ChatColor.GOLD + "Commands: " + ChatColor.YELLOW + "/ic help");
        sender.sendMessage(ChatColor.LIGHT_PURPLE + ChatColor.STRIKETHROUGH + "=========================================");
      } else {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
    }
    else
    {
      ItemStack item;
      if ((args.length >= 2) && (args[0].equalsIgnoreCase("enchant"))) {
        if ((sender instanceof Player)) {
          if ((sender.hasPermission("ImmortalCustom.enchant")) || (sender.hasPermission("ic.enchant"))) {
            Player player = (Player)sender;
            item = player.getItemInHand();
            try {
              int level = Integer.parseInt(args[1]);
              item.addUnsafeEnchantment(Enchantment.LOOT_BONUS_BLOCKS, level);
            } catch (NumberFormatException e) {
              sender.sendMessage(ChatColor.RED + "Second argument must be a number!");
              return false;
            } catch (Exception e) {
              sender.sendMessage(ChatColor.RED + "Error while enchanting this item!");
              return false;
            }
            sender.sendMessage(ChatColor.AQUA + "Successfully enchanted!");
          } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
        }
        else sender.sendMessage(ChatColor.RED + "You have to be a player to execute this command!");
      }
      else if (args[0].equalsIgnoreCase("reload")) {
        if ((sender.hasPermission("ImmortalCustom.reload")) || (sender.hasPermission("ic.reload"))) {
          reloadConfig();
          sender.sendMessage(ChatColor.AQUA + "Successfully reloaded the configuration!");
        } else {
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
      } else if (args[0].equalsIgnoreCase("help")) {
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.STRIKETHROUGH + "==================" + ChatColor.LIGHT_PURPLE + "]" + ChatColor.BLUE + " ImmortalCustom " + ChatColor.LIGHT_PURPLE + "[" + ChatColor.STRIKETHROUGH + "==================" + ChatColor.LIGHT_PURPLE + "]");
        for (String command : getDescription().getCommands().keySet()) {
          if ((getCommand(command).getPermission() == null) || (sender.hasPermission(getCommand(command).getPermission()))) {
            sender.sendMessage(ChatColor.DARK_AQUA + "/" + command + " " + ChatColor.BLUE + getCommand(command).getDescription());
            if (getCommand(command).getPermission() != null) {
              sender.sendMessage(ChatColor.GOLD + "    Permission: " + ChatColor.YELLOW + getCommand(command).getPermission());
            }
          }
        }
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.STRIKETHROUGH + "===================================================" + ChatColor.LIGHT_PURPLE + "]");
      } else if ((args.length >= 2) && (args[0].equalsIgnoreCase("fortunelist"))) {
        if ((sender instanceof Player)) {
          if (args[1].equalsIgnoreCase("add")) {
            if ((sender.hasPermission("ImmortalCustom.fortunelist.add")) || (sender.hasPermission("ic.fortunelist.add"))) {
              if (!((Player)sender).getItemInHand().getType().equals(Material.AIR)) {
                if (!getConfig().getStringList("fortuneBlocks").contains(((Player)sender).getItemInHand().getType().toString())) {
                  List list = getConfig().getStringList("fortuneBlocks");
                  list.add(((Player)sender).getItemInHand().getType().toString());
                  getConfig().set("fortuneBlocks", list);
                  saveConfig();
                  sender.sendMessage(ChatColor.GREEN + "Successfully added " + ((Player)sender).getItemInHand().getType().toString() + " to the fortunelist.");
                } else {
                  sender.sendMessage(ChatColor.RED + "This block is already in the fortunelist.");
                }
              }
              else sender.sendMessage(ChatColor.RED + "You must hold an item in your hand.");
            }
            else
              sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("remove")) {
            if ((sender.hasPermission("ImmortalCustom.fortunelist.remove")) || (sender.hasPermission("ic.fortunelist.remove"))) {
              if (!((Player)sender).getItemInHand().getType().equals(Material.AIR)) {
                if (getConfig().getStringList("fortuneBlocks").contains(((Player)sender).getItemInHand().getType().toString())) {
                  List list = getConfig().getStringList("fortuneBlocks");
                  list.remove(((Player)sender).getItemInHand().getType().toString());
                  getConfig().set("fortuneBlocks", list);
                  saveConfig();
                  sender.sendMessage(ChatColor.GREEN + "Successfully removed " + ((Player)sender).getItemInHand().getType().toString() + " from the fortunelist.");
                } else {
                  sender.sendMessage(ChatColor.RED + "This block is not in the fortunelist.");
                }
              }
              else sender.sendMessage(ChatColor.RED + "You must hold an item in your hand.");
            }
            else
              sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("on")) {
            if ((sender.hasPermission("ImmortalCustom.fortunelist.on")) || (sender.hasPermission("ic.fortunelist.on"))) {
              if (!getConfig().getBoolean("fortunelist")) {
                getConfig().set("fortunelist", Boolean.valueOf(true));
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully turned on the fortunelist.");
              } else {
                sender.sendMessage(ChatColor.RED + "The fortunelist is already turned on.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("off")) {
            if ((sender.hasPermission("ImmortalCustom.fortunelist.off")) || (sender.hasPermission("ic.fortunelist.off"))) {
              if (getConfig().getBoolean("fortunelist")) {
                getConfig().set("fortunelist", Boolean.valueOf(false));
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully turned off the fortunelist.");
              } else {
                sender.sendMessage(ChatColor.RED + "The fortunelist is already turned off.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else
            sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use add, remove, on or off.");
        }
        else
          sender.sendMessage(ChatColor.RED + "You have to be a player to execute this command!");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("pickuplist"))) {
        if ((sender instanceof Player)) {
          if (args[1].equalsIgnoreCase("add")) {
            if ((sender.hasPermission("ImmortalCustom.pickuplist.add")) || (sender.hasPermission("ic.pickuplist.add"))) {
              if (!((Player)sender).getItemInHand().getType().equals(Material.AIR)) {
                if (!getConfig().getStringList("pickupBlocks").contains(((Player)sender).getItemInHand().getType().toString())) {
                  List list = getConfig().getStringList("pickupBlocks");
                  list.add(((Player)sender).getItemInHand().getType().toString());
                  getConfig().set("pickupBlocks", list);
                  saveConfig();
                  sender.sendMessage(ChatColor.GREEN + "Successfully added " + ((Player)sender).getItemInHand().getType().toString() + " to the pickuplist.");
                } else {
                  sender.sendMessage(ChatColor.RED + "This block is already in the pickuplist.");
                }
              }
              else sender.sendMessage(ChatColor.RED + "You must hold an item in your hand.");
            }
            else
              sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("remove")) {
            if ((sender.hasPermission("ImmortalCustom.pickuplist.remove")) || (sender.hasPermission("ic.pickuplist.remove"))) {
              if (!((Player)sender).getItemInHand().getType().equals(Material.AIR)) {
                if (getConfig().getStringList("pickupBlocks").contains(((Player)sender).getItemInHand().getType().toString())) {
                  List list = getConfig().getStringList("pickupBlocks");
                  list.remove(((Player)sender).getItemInHand().getType().toString());
                  getConfig().set("pickupBlocks", list);
                  saveConfig();
                  sender.sendMessage(ChatColor.GREEN + "Successfully removed " + ((Player)sender).getItemInHand().getType().toString() + " from the pickuplist.");
                } else {
                  sender.sendMessage(ChatColor.RED + "This block is not in the pickuplist.");
                }
              }
              else sender.sendMessage(ChatColor.RED + "You must hold an item in your hand.");
            }
            else
              sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("on")) {
            if ((sender.hasPermission("ImmortalCustom.pickuplist.on")) || (sender.hasPermission("ic.pickuplist.on"))) {
              if (!getConfig().getBoolean("pickuplist")) {
                getConfig().set("pickuplist", Boolean.valueOf(true));
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully turned on the pickuplist.");
              } else {
                sender.sendMessage(ChatColor.RED + "The pickuplist is already turned on.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("off")) {
            if ((sender.hasPermission("ImmortalCustom.pickuplist.off")) || (sender.hasPermission("ic.pickuplist.off"))) {
              if (getConfig().getBoolean("pickuplist")) {
                getConfig().set("pickuplist", Boolean.valueOf(false));
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully turned off the pickuplist.");
              } else {
                sender.sendMessage(ChatColor.RED + "The pickuplist is already turned off.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else
            sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use add, remove, on or off.");
        }
        else
          sender.sendMessage(ChatColor.RED + "You have to be a player to execute this command!");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("worlds"))) {
        if ((sender instanceof Player)) {
          if (args[1].equalsIgnoreCase("add")) {
            if ((sender.hasPermission("ImmortalCustom.worlds.add")) || (sender.hasPermission("ic.worlds.add"))) {
              if (!getConfig().getStringList("disabledWorlds").contains(((Player)sender).getWorld().getName())) {
                List list = getConfig().getStringList("activeBlocks");
                list.add(((Player)sender).getWorld().getName());
                getConfig().set("disabledWorlds", list);
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully added " + ((Player)sender).getWorld().getName() + " to the disabled worlds.");
              } else {
                sender.sendMessage(ChatColor.RED + "This world is already in the disabled worlds.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else if (args[1].equalsIgnoreCase("remove")) {
            if ((sender.hasPermission("ImmortalCustom.worlds.remove")) || (sender.hasPermission("ic.worlds.remove"))) {
              if (getConfig().getStringList("disabledWorlds").contains(((Player)sender).getWorld().getName())) {
                List list = getConfig().getStringList("disabledWorlds");
                list.remove(((Player)sender).getWorld().getName());
                getConfig().set("disabledWorlds", list);
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "Successfully removed " + ((Player)sender).getWorld().getName() + " from the disabled worlds.");
              } else {
                sender.sendMessage(ChatColor.RED + "This world is not in the disabled worlds.");
              }
            }
            else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
          }
          else
            sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use add or remove.");
        }
        else
          sender.sendMessage(ChatColor.RED + "You have to be a player to execute this command!");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("multiplier"))) {
        if ((sender.hasPermission("ImmortalCustom.multiplier")) || (sender.hasPermission("ic.multiplier")))
          try {
            getConfig().set("multiplier", Double.valueOf(Double.parseDouble(args[1])));
            saveConfig();
            sender.sendMessage(ChatColor.GREEN + "Successfully set the multiplier to " + Double.parseDouble(args[1]));
          } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Second argument must be a number!");
          }
        else
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("difference"))) {
        if ((sender.hasPermission("ImmortalCustom.multiplier")) || (sender.hasPermission("ic.multiplier")))
          try {
            getConfig().set("difference", Integer.valueOf(Integer.parseInt(args[1])));
            saveConfig();
            sender.sendMessage(ChatColor.GREEN + "Successfully set the difference to " + Integer.parseInt(args[1]));
          } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Second argument must be an integer!");
          }
        else
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("autopickup"))) {
        if (args[1].equalsIgnoreCase("on")) {
          if ((sender.hasPermission("ImmortalCustom.autopickup.on")) || (sender.hasPermission("ic.autopickup.on"))) {
            if (!getConfig().getBoolean("autopickup")) {
              getConfig().set("autopickup", Boolean.valueOf(true));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned on the autopickup.");
            } else {
              sender.sendMessage(ChatColor.RED + "The autopickup is already turned on.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else if (args[1].equalsIgnoreCase("off")) {
          if ((sender.hasPermission("ImmortalCustom.autopickup.off")) || (sender.hasPermission("ic.autopickup.off"))) {
            if (getConfig().getBoolean("autopickup")) {
              getConfig().set("autopickup", Boolean.valueOf(false));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned off the autopickup.");
            } else {
              sender.sendMessage(ChatColor.RED + "The autopickup is already turned off.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else
          sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use on or off.");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("flames"))) {
        if (args[1].equalsIgnoreCase("on")) {
          if ((sender.hasPermission("ImmortalCustom.flames.on")) || (sender.hasPermission("ic.flames.on"))) {
            if (!getConfig().getBoolean("flames")) {
              getConfig().set("flames", Boolean.valueOf(true));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned on the flames when smelting.");
            } else {
              sender.sendMessage(ChatColor.RED + "The flames are already turned on.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else if (args[1].equalsIgnoreCase("off")) {
          if ((sender.hasPermission("ImmortalCustom.flames.off")) || (sender.hasPermission("ic.flames.off"))) {
            if (getConfig().getBoolean("flames")) {
              getConfig().set("flames", Boolean.valueOf(false));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned off the flames when smelting.");
            } else {
              sender.sendMessage(ChatColor.RED + "The flames are already turned off.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else
          sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use on or off.");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("dropdown"))) {
        if (args[1].equalsIgnoreCase("on")) {
          if ((sender.hasPermission("ImmortalCustom.dropdown.on")) || (sender.hasPermission("ic.dropdown.on"))) {
            if (!getConfig().getBoolean("dropdown")) {
              getConfig().set("dropdown", Boolean.valueOf(true));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned on the drop downs.");
            } else {
              sender.sendMessage(ChatColor.RED + "The drop downs are already turned on.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else if (args[1].equalsIgnoreCase("off")) {
          if ((sender.hasPermission("ImmortalCustom.dropdown.off")) || (sender.hasPermission("ic.dropdown.off"))) {
            if (getConfig().getBoolean("dropdown")) {
              getConfig().set("dropdown", Boolean.valueOf(false));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned off the drop downs.");
            } else {
              sender.sendMessage(ChatColor.RED + "The drop downs are already turned off.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else
          sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use on or off.");
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("smeltFortune"))) {
        if ((sender.hasPermission("ImmortalCustom.smeltFortune")) || (sender.hasPermission("ic.smeltFortune")))
          try {
            getConfig().set("smeltFortune", Integer.valueOf(Integer.parseInt(args[1])));
            saveConfig();
            sender.sendMessage(ChatColor.GREEN + "Successfully set the minimum fortune for autosmelt to " + Integer.parseInt(args[1]));
          } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Second argument must be an integer!");
          }
        else
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
      else if (args[0].equalsIgnoreCase("fullmessage")) {
        if ((sender.hasPermission("ImmortalCustom.fullmessage")) || (sender.hasPermission("ic.fullmessage"))) {
          if (args.length > 1) {
            String message = "";
            String[] arrayOfString;
            Exception localException1 = (arrayOfString = args).length; for (e = 0; e < localException1; e++) { String word = arrayOfString[e]; message = message + " " + word; }
            getConfig().set("fullmessage", message.substring(1));
            saveConfig();
            sender.sendMessage(ChatColor.GREEN + "Successfully set the fullmessage.");
          } else {
            getConfig().set("fullmessage", "");
            saveConfig();
            sender.sendMessage(ChatColor.GREEN + "Successfully removed the fullmessage.");
          }
        }
        else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
      else if ((args.length >= 2) && (args[0].equalsIgnoreCase("orestoblocks"))) {
        if (args[1].equalsIgnoreCase("on")) {
          if ((sender.hasPermission("ImmortalCustom.orestoblocks.on")) || (sender.hasPermission("ic.orestoblocks.on"))) {
            if (!getConfig().getBoolean("orestoblocks")) {
              getConfig().set("orestoblocks", Boolean.valueOf(true));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned on the orestoblocks.");
            } else {
              sender.sendMessage(ChatColor.RED + "The orestoblocks is already turned on.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else if (args[1].equalsIgnoreCase("off")) {
          if ((sender.hasPermission("ImmortalCustom.orestoblocks.off")) || (sender.hasPermission("ic.orestoblocks.off"))) {
            if (getConfig().getBoolean("orestoblocks")) {
              getConfig().set("orestoblocks", Boolean.valueOf(false));
              saveConfig();
              sender.sendMessage(ChatColor.GREEN + "Successfully turned off the orestoblocks.");
            } else {
              sender.sendMessage(ChatColor.RED + "The orestoblocks is already turned off.");
            }
          }
          else sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
        }
        else
          sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use on or off.");
      }
      else if (args[0].equalsIgnoreCase("override")) {
        if ((sender.hasPermission("ImmortalCustom.override")) || (sender.hasPermission("ic.override"))) {
          if ((sender instanceof Player)) {
            if (this.WE != null) {
              Selection s = ((WorldEditPlugin)this.WE).getSelection((Player)sender);
              if (s != null) {
                if (getConfig().getStringList("disabledWorlds").contains(s.getWorld().getName())) {
                  String select = s.getMinimumPoint().getBlockX() + " " + s.getMinimumPoint().getBlockY() + " " + s.getMinimumPoint().getBlockZ();
                  select = select + " & " + s.getMaximumPoint().getBlockX() + " " + s.getMaximumPoint().getBlockY() + " " + s.getMaximumPoint().getBlockZ();
                  List list = getConfig().getStringList("areas");
                  if (!list.contains(select)) {
                    list.add(select);
                    getConfig().set("areas." + s.getWorld().getName(), list);
                    saveConfig();
                    sender.sendMessage(ChatColor.GREEN + "ImmortalCustom is now active in this area!");
                  } else {
                    sender.sendMessage(ChatColor.RED + "ImmortalCustom is already active in this area!");
                  }
                } else {
                  sender.sendMessage(ChatColor.RED + "ImmortalCustom is already active in this area!");
                }
              }
              else sender.sendMessage(ChatColor.RED + "Please select a region first."); 
            }
            else
            {
              sender.sendMessage(ChatColor.RED + "You must have WorldEdit installed!");
            }
          }
          else sender.sendMessage(ChatColor.RED + "You have to be a player to execute this command!");
        }
        else
          sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("permission")));
      }
      else {
        sender.sendMessage(ChatColor.RED + "Invalid command parameters! Use /help ImmortalCustom");
      }
    }
    return false;
  }

  @EventHandler(priority=EventPriority.MONITOR)
  public void onBlockBreak(BlockBreakEvent event) {
    if ((!event.isCancelled()) && (!event.getPlayer().getGameMode().equals(GameMode.CREATIVE))) {
      String before = ChatColor.translateAlternateColorCodes('&', getConfig().getString("blocksbroken"));
      boolean isTool = Enchantment.DURABILITY.canEnchantItem(event.getPlayer().getItemInHand());
      if ((!before.isEmpty()) && (event.getPlayer().getItemInHand().hasItemMeta()) && (isTool)) {
        ItemMeta meta = event.getPlayer().getItemInHand().getItemMeta();
        if (meta.getLore() != null) {
          if (((String)meta.getLore().get(0)).matches(before + "[0-9]+")) {
            String number = ((String)meta.getLore().get(0)).substring(before.length());
            int newnumber = Integer.parseInt(number) + 1;
            List lores = meta.getLore();
            lores.set(0, ((String)lores.get(0)).replaceAll(number, newnumber));
            meta.setLore(lores);
          } else {
            List lores = meta.getLore();
            lores.add(0, before + "1");
            meta.setLore(lores);
          }
        } else {
          List lores = new ArrayList();
          lores.add(0, before + "1");
          meta.setLore(lores);
        }
        event.getPlayer().getItemInHand().setItemMeta(meta);
      }
      if (isActive(event.getBlock().getLocation())) {
        ArrayList drops = (ArrayList)event.getBlock().getDrops(event.getPlayer().getItemInHand());
        int exp = event.getExpToDrop();
        int fortune = 0;
        if (event.getPlayer().getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS))
          fortune = event.getPlayer().getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        ItemStack newdrop;
        if ((fortune >= getConfig().getInt("smeltFortune")) && ((event.getPlayer().hasPermission("ImmortalCustom.autosmelt")) || (event.getPlayer().hasPermission("ic.autosmelt")))) {
          for (ItemStack drop : drops) {
            if (getConfig().isSet("smelts." + drop.getType().toString())) {
              newdrop = drop; newdrop.setType(Material.getMaterial(getConfig().getString("smelts." + drop.getType().toString())));
              drops.set(drops.indexOf(drop), newdrop);
              exp += 10;
              if (getConfig().getBoolean("flames")) {
                event.getBlock().getWorld().playEffect(event.getBlock().getLocation().add(0.5D, 1.0D, 0.5D), Effect.MOBSPAWNER_FLAMES, 0);
              }
            }
          }
        }
        if (fortune > 0) {
          int loots = (int)(Math.floor(fortune * getConfig().getDouble("multiplier")) + 1.0D);
          for (newdrop = drops.iterator(); newdrop.hasNext(); ) { drop = (ItemStack)newdrop.next();
            if ((getConfig().getStringList("fortuneBlocks").contains(((ItemStack)drop).getType().toString())) || (!getConfig().getBoolean("fortunelist"))) {
              if ((new Random().nextBoolean()) || (((ItemStack)drop).getAmount() <= getConfig().getInt("difference"))) {
                ItemStack newdrop = (ItemStack)drop;
                ((ItemStack)drop).setAmount(((ItemStack)drop).getAmount() * loots + new Random().nextInt(getConfig().getInt("difference")));
                drops.set(drops.indexOf(drop), newdrop);
              } else {
                ItemStack newdrop = (ItemStack)drop;
                ((ItemStack)drop).setAmount(((ItemStack)drop).getAmount() * loots - new Random().nextInt(getConfig().getInt("difference")));
                drops.set(drops.indexOf(drop), newdrop);
              }
            }
          }
        }
        for (Object drop = drops.iterator(); ((Iterator)drop).hasNext(); ) { ItemStack drop = (ItemStack)((Iterator)drop).next();
          if (((event.getPlayer().hasPermission("ImmortalCustom.autopickup")) || (event.getPlayer().hasPermission("ic.autopickup"))) && (getConfig().getBoolean("autopickup"))) {
            if ((getConfig().getStringList("pickupBlocks").contains(drop.getType().toString())) || (!getConfig().getBoolean("pickuplist"))) {
              if (canGive(event.getPlayer(), drop)) {
                event.getPlayer().getInventory().addItem(new ItemStack[] { drop });
                this.counts.remove(event.getPlayer());
              } else if ((!this.counts.containsKey(event.getPlayer())) || (((Integer)this.counts.get(event.getPlayer())).intValue() == 0)) {
                String[] conf = getConfig().getString("fullsound").split(" ");
                event.getPlayer().playSound(event.getPlayer().getEyeLocation(), Sound.valueOf(conf[0]), Integer.parseInt(conf[1]), Integer.parseInt(conf[2]));
                if (!getConfig().getString("fullmessage").isEmpty()) {
                  event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("fullmessage")));
                }
                if (((event.getPlayer().hasPermission("ImmortalCustom.dropdown")) || (event.getPlayer().hasPermission("ic.dropdown"))) && (getConfig().getBoolean("dropdown"))) {
                  event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
                }
                this.counts.put(event.getPlayer(), Integer.valueOf(1));
              } else {
                this.counts.put(event.getPlayer(), Integer.valueOf(((Integer)this.counts.get(event.getPlayer())).intValue() + 1));
                if (((Integer)this.counts.get(event.getPlayer())).intValue() >= 10)
                  this.counts.put(event.getPlayer(), Integer.valueOf(0));
              }
            }
            else
              event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
          }
          else {
            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
          }
        }
        if (exp > 0) {
          if (((event.getPlayer().hasPermission("ImmortalCustom.autopickup")) || (event.getPlayer().hasPermission("ic.autopickup"))) && (getConfig().getBoolean("autopickup")))
            event.getPlayer().giveExp(exp);
          else {
            ((ExperienceOrb)event.getBlock().getWorld().spawn(event.getBlock().getLocation(), ExperienceOrb.class)).setExperience(exp);
          }
        }
        event.getBlock().setType(Material.AIR);
        event.setCancelled(true);
        if (isTool) {
          ItemStack item = event.getPlayer().getItemInHand();
          if (!item.containsEnchantment(Enchantment.DURABILITY))
            item.setDurability((short)(item.getDurability() + 1));
          else if (new Random().nextInt(item.getEnchantmentLevel(Enchantment.DURABILITY)) == 0) {
            item.setDurability((short)(item.getDurability() + 1));
          }
          if (item.getDurability() >= item.getType().getMaxDurability()) {
            new PlayerItemBreakEvent(event.getPlayer(), item);
            item.setType(Material.AIR);
          }
          event.getPlayer().setItemInHand(item);
        }
      }
    }
  }

  @EventHandler(priority=EventPriority.HIGHEST)
  public void onPlayerInteract(PlayerInteractEvent event)
  {
    if (((event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) || (event.getAction().equals(Action.RIGHT_CLICK_AIR))) && 
      ((event.getPlayer().hasPermission("ImmortalCustom.orestoblocks")) || (event.getPlayer().hasPermission("ic.orestoblocks"))) && (getConfig().getBoolean("orestoblocks")) && 
      (event.getPlayer().isSneaking()) && (!isClickable(event.getClickedBlock()))) {
      for (ItemStack material : this.materials.keySet()) {
        int ores = 0;
        for (Integer slot : event.getPlayer().getInventory().all(material.getType()).keySet()) {
          ores += event.getPlayer().getInventory().getItem(slot.intValue()).getAmount();
        }
        if (ores >= material.getAmount()) {
          int blocks = (ores - ores % material.getAmount()) / material.getAmount();
          event.getPlayer().getInventory().remove(material.getType());
          if (ores % material.getAmount() > 0) {
            event.getPlayer().getInventory().addItem(new ItemStack[] { new ItemStack(material.getType(), ores % material.getAmount(), material.getDurability()) });
          }
          event.getPlayer().getInventory().addItem(new ItemStack[] { new ItemStack((Material)this.materials.get(material), blocks) });
          String[] conf = getConfig().getString("orestoblocksound").split(" ");
          event.getPlayer().playSound(event.getPlayer().getEyeLocation(), Sound.valueOf(conf[0]), Integer.parseInt(conf[1]), Integer.parseInt(conf[2]));
        }
      }
      event.getPlayer().updateInventory();
      event.setCancelled(true);
    }
  }

  public boolean isActive(Location loc)
  {
    if (!getConfig().getStringList("disabledWorlds").contains(loc.getWorld().getName()))
      return true;
    if (getConfig().isSet("areas." + loc.getWorld().getName())) {
      for (String select : getConfig().getStringList("areas." + loc.getWorld().getName())) {
        if (select.matches("\\-?[0-9]+ \\-?[0-9]+ \\-?[0-9]+ & \\-?[0-9]+ \\-?[0-9]+ \\-?[0-9]+")) {
          String[] pos1 = select.split(" & ")[0].split(" ");
          String[] pos2 = select.split(" & ")[1].split(" ");
          if ((loc.getX() >= Integer.parseInt(pos1[0])) && (loc.getY() >= Integer.parseInt(pos1[1])) && (loc.getZ() >= Integer.parseInt(pos1[2])) && 
            (loc.getX() <= Integer.parseInt(pos2[0])) && (loc.getY() <= Integer.parseInt(pos2[1])) && (loc.getZ() <= Integer.parseInt(pos2[2]))) {
            return true;
          }
        }
      }
    }

    return false;
  }

  public boolean canGive(Player player, ItemStack item) {
    if (player.getInventory().firstEmpty() > -1) {
      return true;
    }
    if (item.getType().getMaxStackSize() > 1) {
      HashMap items = player.getInventory().all(item.getType());
      int space = 0;
      for (ItemStack slot : items.values()) {
        space += item.getType().getMaxStackSize() - slot.getAmount();
        if (space >= item.getAmount()) {
          return true;
        }
      }
    }
    return false;
  }

  public static boolean isClickable(Block type) {
    if ((type == null) || (type.getType().equals(Material.AIR))) return false;
    switch ($SWITCH_TABLE$org$bukkit$Material()[type.getType().ordinal()]) {
    case 24:
    case 26:
    case 27:
    case 59:
    case 62:
    case 63:
    case 64:
    case 65:
    case 70:
    case 72:
    case 78:
    case 85:
    case 93:
    case 94:
    case 95:
    case 98:
    case 109:
    case 118:
    case 119:
    case 124:
    case 132:
    case 139:
    case 140:
    case 145:
    case 147:
    case 148:
    case 151:
    case 152:
    case 156:
    case 160:
    case 240:
      return true;
    }
    return false;
  }
}