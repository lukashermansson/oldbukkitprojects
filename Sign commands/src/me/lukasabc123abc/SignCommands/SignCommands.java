package me.lukasabc123abc.SignCommands;

import java.util.logging.Logger;

import net.minecraft.server.v1_10_R1.Block;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class SignCommands extends JavaPlugin{
	public final Logger logger = Logger.getLogger("minecraft");
	public static SignCommands plugin;
	public void onDisable() {
			PluginDescriptionFile pdfFile = this.getDescription();
			Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Disabled!");
	}
	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getServer().getLogger().info("[" + pdfFile.getName() + "]" + " Has been Enabled!");	
	}
	
	
	@EventHandler
	public boolean onclick(PlayerInteractEvent event){
		if(event.getAction() == Action.LEFT_CLICK_BLOCK){
			if(event.getClickedBlock() == Block.getById(63)){
				Inventory inv = Bukkit.getServer().createInventory(null, 9*6, ChatColor.GOLD + "GuiCommands");
				event.getPlayer().openInventory(inv);
			}
		}
		
		return false;
		
	}
}
